
{ UTF8Logger unit
  ---------------
  Simple class TUTF8Logger to write log file.
  Correctly works with UTF-8 strings in Format()
}
unit UTF8Logger;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, LazFileUtils, LazUTF8;

type

  { TUTF8Logger }

  TUTF8Logger     = class
  const
    TSFMT_DEFAULT = 'yyyy.mm.dd hh:nn:ss.zzz';

  private
    FActive:       Boolean;
    FAppendMode:   Boolean;
    FTimeStampFmt: String;
    FID:           String;
    FFilename:     String;
    FFile:         Text;

    procedure SetActive(AValue: Boolean);
    procedure SetAppendMode(AValue: Boolean);
    procedure SetTimeStampFmt(AValue: String);
    procedure SetID(AValue: String);
    procedure SetFilename(AValue: String);

  public
    constructor Create(AFilename: String = ''; AID: String = '');
    destructor Destroy; override;

    procedure Log(const Msg: String);
    procedure Log(const Fmt: String; Args: array of const);

    property Active: Boolean read FActive write SetActive;
    property AppendMode: Boolean read FAppendMode write SetAppendMode;
    property TimeStampFmt: String read FTimeStampFmt write SetTimeStampFmt;
    property ID: String read FID write SetID;
    property Filename: String read FFilename write SetFilename;

  end;


implementation


{ TUTF8Logger }

procedure TUTF8Logger.SetActive(AValue: Boolean);
  begin
    if FActive = AValue then Exit;
    FActive := AValue;

    if FFilename = '' then FActive := False;

    if FActive then
      if not FileExistsUTF8(FFilename) or not FAppendMode then
        begin
        Assign(FFile, FFilename);
        Rewrite(FFile);
        Write(FFile, #239#187#191); // UTF-8 start marker EF BB BF
        Close(FFile);
        end;
  end;

procedure TUTF8Logger.SetAppendMode(AValue: Boolean);
  begin
    if FAppendMode = AValue then Exit;
    FAppendMode := AValue;
  end;

procedure TUTF8Logger.SetFilename(AValue: String);
  var
    tmp: Boolean;
  begin
    if FFilename = AValue then Exit;
    tmp       := Active;
    Active    := False;
    FFilename := AValue;
    Active    := tmp;
  end;

procedure TUTF8Logger.SetID(AValue: String);
  begin
    if FID = AValue then Exit;
    FID := AValue;

    if FID <> '' then FID += ' ';
  end;

procedure TUTF8Logger.SetTimeStampFmt(AValue: String);
  begin
    if FTimeStampFmt = AValue then Exit;
    FTimeStampFmt := AValue;

    if FTimeStampFmt = '' then FTimeStampFmt := TSFMT_DEFAULT;
  end;

constructor TUTF8Logger.Create(AFilename: String; AID: String);
  begin
    FTimeStampFmt := TSFMT_DEFAULT;
    FID           := AID;
    FFilename     := AFilename;
    FAppendMode   := True;
    FActive       := False;
  end;

destructor TUTF8Logger.Destroy;
  begin
    Active := False;

    inherited Destroy;
  end;

procedure TUTF8Logger.Log(const Msg: String);
  begin
    if not FActive then Exit;

    Assign(FFile, FFilename);

    if FileExistsUTF8(FFilename) then
      Append(FFile) else
      Rewrite(FFile);

    Writeln(FFile, Format('%s[%s] %s', [
      ID, FormatDateTime(FTimeStampFmt, Now), Msg]));

    Close(FFile);
  end;

procedure TUTF8Logger.Log(const Fmt: String; Args: array of const);
  begin
    Log(UnicodeFormat(Fmt, Args));
  end;


end.
