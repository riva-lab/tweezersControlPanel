unit appAbout;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, StdCtrls, LCLType, LCLIntf, LazFileUtils, Registry,
  OnlineUpdater, ouVersion, u_helpers, AppLocalizer;


resourcestring
  ABOUT_VERSION = 'version %s';
  ABOUT_BIT     = '%s-bit';
  ABOUT_BUILD   = 'build #%d, %s';


const
  ABOUT_RIGHTS        = 'FreeBSD License';
  ABOUT_OPENSRC       = 'Free Open Source Software';

  FILE_LICENSE        = 'license.md';
  FILE_README         = 'readme.md';
  FILE_HELP           = 'help' + DirectorySeparator + 'tweezersCP-help.md';

  // homepage url, e.g. https://example.site/home,
  APP_HOME_ADDRESS    = 'https://riva-lab.gitlab.io/en/apps/tweezersControlPanel/';

  // project repository url
  APP_REPO_ADDRESS    = 'https://gitlab.com/riva-lab/tweezersControlPanel/';

  // hardware webpage url
  APP_OTHER_ADDRESS   = 'https://gitlab.com/Andrey_71B/bm8232-rlc-meter-tweezers-hw/';

  // CH341 driver url
  APP_DRIVER_ADDRESS  = 'https://www.wch-ic.com/downloads/CH341SER_EXE.html';

  // Android app homepage url
  APP_ANDROID_ADDRESS = 'https://github.com/si2la/BM8232-Tweezers/';


procedure linkClick(Sender: TObject);
function UpdateAboutAppShort: String;
function UpdateAboutAppInfo: String;

var
  AppVersionStr: String = '';
  AppProductStr: String = '';
  AppSystemStr:  String = '';


implementation


procedure linkClick(Sender: TObject);

  function TryOpenFile(AFilename: String): Boolean;
    begin
      Result := False;
      if OpenDocument(AFilename) then Exit(True);
      if OpenDocument('..' + DirectorySeparator + AFilename) then Exit(True);
    end;

  function GetFromResources(ARes: String; AFilename: String): Boolean;
    begin
        try
        with TResourceStream.Create(HINSTANCE, ARes, RT_RCDATA) do
          try
          SaveToFile(AFilename);
          finally
          Free;
          OpenDocument(AFilename); // open file
          Sleep(2000);             // wait for 2s while file is opening
          DeleteFile(AFilename);   // delete file
          end;
        Result := True;
        except
        Result := False;
        end;
    end;
  var
    url, id: String;
  begin
    if TComponent(Sender).ClassName <> 'TLabel' then Exit;
    url := TLabel(Sender).Hint;
    id  := ExtractFileNameWithoutExt(url);

    if url.StartsWith('https://') then
      OpenURL(url)
    else
    if not TryOpenFile(id + '.' + appLocalizerEx.CurrentLangCode + '.html') then
      if not TryOpenFile(id + '.' + appLocalizerEx.CurrentLangCode + '.md') then
        if not TryOpenFile(id + '.html') then
          if not TryOpenFile(id + '.md') then
            GetFromResources(id, url);
  end;

function UpdateAboutAppShort: String;
  var
    _bldTime: TDateTime;
    _info:    TStringList;
    _arc:     String;
  begin
    _info  := TStringList.Create;
    _arc   := '';
    Result := '';

    {$IfDef WINDOWS}
      {$IfDef WIN64}
      _arc := Format(ABOUT_BIT, ['64']);
      {$EndIf}
      {$IfDef WIN32}
      _arc := Format(ABOUT_BIT, ['32']);
      {$EndIf}

      with TRegistry.Create(KEY_READ) do
        begin
          try
          RootKey      := HKEY_LOCAL_MACHINE;
          OpenKeyReadOnly('SYSTEM\CurrentControlSet\Control\Session Manager\Environment');
          AppSystemStr := ReadString('PROCESSOR_ARCHITECTURE').Contains('64').Select('x64', 'x32');
          CloseKey;
          OpenKeyReadOnly('SOFTWARE\Microsoft\Windows NT\CurrentVersion');
          AppSystemStr := Format('%s %s Version %s Build %s.%d', [
            ReadString('ProductName'),
            AppSystemStr,
            ReadString('DisplayVersion').IsEmpty.Select(
              ReadString('ReleaseId'),
              ReadString('DisplayVersion') + ' (' + ReadString('ReleaseId') + ')'),
            ReadString('CurrentBuild'),
            ReadInteger('UBR')]);
          except
          end;
        Free;
        end;
    {$EndIf}

    with TFileVersionInfoSimple.Create do
      try
      if Assigned(ReadVersionInfo) then
        begin
        _info.Add(InternalName);

        with ParseVersion(FileVersion) do
          begin
          TryStrToDate({$INCLUDE %DATE%}, _bldTime, 'YYYY/MM/DD', '/');
          AppVersionStr := Format('%d.%d.%d', [Major, Minor, Revision]);
          _info.Add(Format(ABOUT_VERSION, [AppVersionStr]) + (_arc <> '').Select(', ' + _arc, ''));
          _info.Add(Format(ABOUT_BUILD, [Build, FormatDateTime('yyyy.mm.dd', _bldTime)]));
          end;

        Result := _info.Text;
        end;
      finally
      Free;
      end;

    _info.Free;
  end;

function UpdateAboutAppInfo: String;
  var
    _info: TStringList;
  begin
    _info  := TStringList.Create;
    Result := '';

    with TFileVersionInfoSimple.Create do
      try
      if Assigned(ReadVersionInfo) then
        begin
        _info.Add('');
        _info.Add(FileDescription);
        _info.Add('');
        _info.Add('© ' + LegalCopyright);
        _info.Add(CompanyName);
        _info.Add('');
        _info.Add(ABOUT_RIGHTS);
        _info.Add('');
        _info.Add(ABOUT_OPENSRC);

        Result := UpdateAboutAppShort + _info.Text;

        AppProductStr := ProductName;
        end;
      finally
      Free;
      end;

    _info.Free;
  end;


end.
