unit resHelper;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Windows, Graphics;


// get the specified resource RCDATA as a string
function GetResourceAsString(AResName: String): String;

// load picture from the specified resource RCDATA
procedure LoadPictureFromResource(ABitmap: TPicture; AResName: String);


implementation


function GetResourceAsString(AResName: String): String;
  var
    resStream: TResourceStream;
  begin
    // create a resource stream which points to our resource
    resStream := TResourceStream.Create(HInstance, AResName, RT_RCDATA);

      try
      with TStringStream.Create do
        try
        LoadFromStream(resStream);
        Result := DataString;
        Free;
        except
        Result := '';
        end;

      finally
      resStream.Free; // destroy the resource stream
      end;
  end;

procedure LoadPictureFromResource(ABitmap: TPicture; AResName: String);
  var
    resStream: TResourceStream;
  begin
    // create a resource stream which points to our resource
    resStream := TResourceStream.Create(HInstance, AResName, RT_RCDATA);

      try
        try
        ABitmap.LoadFromStream(resStream);
        except
        end;

      finally
      resStream.Free; // destroy the resource stream
      end;
  end;


end.
