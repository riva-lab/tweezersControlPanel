unit deviceNumbers;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, modelTypes, regexpr;


const
  SEP_DECIMAL: array[TTweezersDecimal] of String   = ('.', ',');
  SEP_THOUSAND: array[TTweezersThousand] of String = ('+', '', ',', '.', ' ');


// modify number represented by string
function ModifyNumber(ANumber: String; ADecSep: TTweezersDecimal = tdDot; AKiloSep: TTweezersThousand = ttNone; TrimZeros: Boolean = False): String;

// modify number represented by string
function ModifyNumber(ANumber: String; ADecSep: String = '.'; AKiloSep: String = ''; TrimZeros: Boolean = False): String;


implementation


function ModifyNumber(ANumber: String; ADecSep: TTweezersDecimal; AKiloSep: TTweezersThousand; TrimZeros: Boolean): String;
  begin
    Result := ModifyNumber(ANumber, SEP_DECIMAL[ADecSep], SEP_THOUSAND[AKiloSep], TrimZeros);
  end;

function ModifyNumber(ANumber: String; ADecSep: String; AKiloSep: String; TrimZeros: Boolean): String;

  function PutSeparators(S: String; IsInt: Boolean = True): String;
    var
      i: Integer;
    begin
      Result := S;

      if AKiloSep = '+' then
        AKiloSep := '' else
        Result   := Result.Replace(' ', '');

      if TrimZeros then
        if IsInt then
          Result := Result.TrimLeft(['0']) else
          Result := Result.TrimRight(['0']);

      if Length(Result) = 0 then
        if IsInt then
          Exit('0') else
          Exit('');

      with TRegExpr.Create do
        begin
        if IsInt then
          Expression := '^(\d{0,3}?\s?)(\d{0,3}?\s?)(\d{0,3}?\s?)(\d{1,3})$' else
          Expression := '^(\d{1,3}\s?)(\d{0,3}\s?)(\d{0,3}\s?)(\d{0,3})$';
        InputString := Result;
        Result := '';

        if Exec then
          for i := 1 to SubExprMatchCount do
            Result += String(Match[i]) + AKiloSep;

        Free;
        end;

      Result := Result.Trim(AKiloSep.ToCharArray);
    end;
  begin
    with TRegExpr.Create do
      begin
      Expression  := '(\+|\-)?((?:\d ?)+)?(?:(\.)((?:\d ?)+))?([^\d\s]+)?';
      InputString := ANumber;

      if not Exec then
        Result := ANumber
      else
      if String(Match[1] + Match[2] + Match[3] + Match[4]) = '' then
        Result := ANumber
      else
        begin
        Result := String(Match[1]);
        Result += PutSeparators(String(Match[2]));
        Result += String(Match[3]).Replace('.', ADecSep);
        Result += PutSeparators(String(Match[4]), False);
        Result += String(Match[5]);
        end;

      Free;
      end;
  end;


end.
