unit modelTypes;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils;

type
  TTweezersStatus    = (tsOffline, tsOnline, tsScanning);
  TTweezersMode      = (tmRLC, tmVoltmeter, tmDiode, tmGenerator);
  TTweezersEqCircuit = (teAuto, teParallel, teSerial);
  TTweezersMeasFreq  = (tfAuto, tf95, tf1k, tf10k, tf95k, tf160k);
  TTweezersBand      = (tbAuto, tb1, tb2, tb3);
  TTweezersOffset    = (toDisable, to6V, to26V);
  TTweezersGenMode   = (tgSine30, tgSine300, tgSine3000, tgPwm50, tgPwm2k, tgPwm50k, tgUart, tgNoise);
  TTweezersRelative  = Boolean;

  TTweezersItem     = (tiUnknown, tiResistor, tiCapacitor, tiInductor);
  TTweezersUnits    = (tuOriginal, tuStandard, tuCyrillic, tuLocalize);
  TTweezersDecimal  = (tdDot, tdComma);
  TTweezersThousand = (ttOriginal, ttNone, ttComma, ttDot, ttSpace);
  TTweezersCalType  = (tcRes12, tcRes3, tcUlow, tcUhigh, tcXtal, tcOS, tcOSL, tcNone);
  TTweezersCalStage = (tcsStart, tcsOpenTips, tcsShortTips, tcsRes12, tcsRes3, tcsWait, tcsDone, tcsError);


const
  STR_TWZ_GENM: array[TTweezersGenMode] of String =
    ('Sin 30mV', 'Sin 300mV', 'Sin 3V', 'PWM 50Hz', 'PWM 2kHz', 'PWM 50kHz', 'Serial', 'Noise');

  STR_TWZ_MODE: array[TTweezersMode] of String =
    ('RLC-meter', 'Voltmeter', 'Diode test', 'Generator');


implementation

end.
