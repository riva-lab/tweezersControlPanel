unit comm;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, synaser, synautil, SerialPortUtils;

type

  { TSerialComm }

  TSerialComm = class(TThread)
  const
    DEFAULT_TIMEOUT_BREAK = 800;    // [ms]
    DEFAULT_TIMEOUT       = 50;     // [ms]
    DEFAULT_BAUDRATE      = 115200; // [bods/s]
    DEFAULT_DATA_BITS     = 8;
    DEFAULT_PARITY        = 'N';
    DEFAULT_STOP_BITS     = SB1;

  private
    FSerial:    TBlockSerial;
    FConnected: Boolean;
    FStart:     Boolean;
    FPort:      String;
    FMessage:   String;
    FRequest:   TStringList;

    function NextPort: String;
    function DoDataExchange: Boolean;
    function IsRxMagic: Boolean;
    function IsConnected: Boolean;

    procedure Execute; override;
    procedure SetPort(AValue: String);
    procedure SetRequest(AValue: String);

    function GetRequest: String;
    function GetMessage: String;

  public
    Baudrate:    Integer;
    Timeout:     Integer;
    Autoconnect: Boolean;
    Magic:       TStringArray;

    OnRequest:    TThreadMethod;
    OnMessage:    TThreadMethod;
    OnConnect:    TThreadMethod;
    OnDisconnect: TThreadMethod;

    constructor Create;
    destructor Destroy; override;

    property Port: String read FPort write SetPort;
    property Connected: Boolean read FConnected;
    property Message: String read GetMessage;
    property Request: String write SetRequest;
  end;


implementation


{ TSerialComm }

function TSerialComm.NextPort: String;
  var
    i: Integer;
  begin
    Result := FPort;

    with SerialEnumerator do
      if Count > 0 then
        try
        for i := 0 to Count - 1 do
          if Strings[i] = FPort then
            if i = Count - 1 then
              Break
            else
              Exit(Strings[i + 1]);

        Result := Strings[0];
        except
        end;
  end;

function TSerialComm.DoDataExchange: Boolean;
  var
    tStart: LongWord;
  begin
    tStart := GetTick;

    while (GetTick - tStart < Timeout) and FConnected do
      begin
      if FSerial.CanRead(DEFAULT_TIMEOUT div 4) then
        begin
        Sleep(DEFAULT_TIMEOUT);
        FMessage := FSerial.RecvPacket(0);
        Synchronize(OnMessage);
        Exit(True);
        end;

      if FRequest.Count > 0 then
        if FSerial.CanWrite(DEFAULT_TIMEOUT) then
          begin
          FSerial.SendString(GetRequest);
          Synchronize(OnRequest);
          Exit(True);
          end;
      end;

    Result := False;
  end;

function TSerialComm.IsRxMagic: Boolean;
  var
    _cycles:  Integer;
    _data, m: String;
  begin
    if Length(Magic) = 0 then Exit(True);

    Result  := False;
    _cycles := Timeout div DEFAULT_TIMEOUT;

    while (_cycles > 0) and not Result and not FStart do
      try
      Dec(_cycles);

      if FSerial.CanRead(0) then
        begin
        Sleep(DEFAULT_TIMEOUT);
        _data := FSerial.RecvPacket(0);

        for m in Magic do
          Result := Result or _data.Contains(m);
        end
      else
        Sleep(DEFAULT_TIMEOUT);

      except
      end;
  end;

function TSerialComm.IsConnected: Boolean;
  begin
    Result := False;

    with FSerial do
      try
      Purge;
      CloseSocket;
      FRequest.Clear;

      if (FPort <> '') and IsSerialPortFree(FPort) then
        begin
        Connect(FPort);
        Config(Baudrate, DEFAULT_DATA_BITS, DEFAULT_PARITY, DEFAULT_STOP_BITS, False, False);
        end;

        try
        Result := FSerial.Handle <> System.THandle(-1);
        except
        end;

      except
      Flush;
      Purge;
      CloseSocket;
      end;
  end;

procedure TSerialComm.Execute;
  begin
    while not Terminated do
      begin
      if FStart then
        begin
        FStart     := False;
        FConnected := IsConnected and IsRxMagic;
        if FConnected then Synchronize(OnConnect);
        end;

      if FConnected then
        begin
        Autoconnect := False;
        FConnected  := DoDataExchange;
        if not FConnected then Synchronize(OnDisconnect);
        end
      else
      if Autoconnect then
        Port := NextPort
      else
        Port := FPort;

      Sleep(10);
      end;
  end;

procedure TSerialComm.SetPort(AValue: String);
  begin
    FPort      := AValue;
    FConnected := False;
    FStart     := True;
  end;

procedure TSerialComm.SetRequest(AValue: String);
  begin
    FRequest.Add(AValue);
  end;

function TSerialComm.GetMessage: String;
  begin
    Result   := FMessage;
    FMessage := '';
  end;

function TSerialComm.GetRequest: String;
  begin
    if FRequest.Count = 0 then
      Result := ''
    else
      begin
      Result := FRequest[0];
      FRequest.Delete(0);
      end;
  end;

constructor TSerialComm.Create;
  begin
    FPort       := '';
    FMessage    := '';
    FRequest    := TStringList.Create;
    FStart      := False;
    FConnected  := False;
    Autoconnect := False;
    Baudrate    := DEFAULT_BAUDRATE;
    Timeout     := DEFAULT_TIMEOUT_BREAK;

    Magic        := [];
    SetLength(Magic, 0);
    OnConnect    := nil;
    OnDisconnect := nil;
    OnMessage    := nil;
    OnRequest    := nil;

    FSerial                 := TBlockSerial.Create;
    FSerial.DeadlockTimeout := Timeout;

    inherited Create(False);
  end;

destructor TSerialComm.Destroy;
  begin
    FSerial.Free;
    FRequest.Free;
    inherited Destroy;
  end;


end.
