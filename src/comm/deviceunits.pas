unit deviceUnits;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, modelTypes, i18n;

const

  { Device units }

  DEF_UNIT_R1 = 'mΩ';
  DEF_UNIT_R2 = 'kΩ';
  DEF_UNIT_R3 = 'MΩ';
  DEF_UNIT_R4 = 'GΩ';
  DEF_UNIT_R5 = 'Ω';

  DEF_UNIT_C1 = 'pF';
  DEF_UNIT_C2 = 'nF';
  DEF_UNIT_C3 = 'uF';
  DEF_UNIT_C4 = 'mF';
  DEF_UNIT_C5 = 'F';

  DEF_UNIT_L1 = 'nH';
  DEF_UNIT_L2 = 'uH';
  DEF_UNIT_L3 = 'mH';
  DEF_UNIT_L4 = 'kH';
  DEF_UNIT_L5 = 'H';

  DEF_UNIT_V1 = 'mV';
  DEF_UNIT_V2 = 'V';

  DEF_UNIT_F1 = 'kHz';
  DEF_UNIT_F2 = 'MHz';
  DEF_UNIT_F3 = 'Hz';

  DEF_UNIT_B1 = 'bps';

  DEF_UNIT_D1 = '%';

  DEF_UNIT_T1 = 'ns';
  DEF_UNIT_T2 = 'us';
  DEF_UNIT_T3 = 'ms';
  DEF_UNIT_T4 = 's';


  { Standard / international units }

  STD_UNIT_R1 = 'mΩ';
  STD_UNIT_R2 = 'kΩ';
  STD_UNIT_R3 = 'MΩ';
  STD_UNIT_R4 = 'GΩ';
  STD_UNIT_R5 = ' Ω';

  STD_UNIT_C1 = 'pF';
  STD_UNIT_C2 = 'nF';
  STD_UNIT_C3 = 'µF';
  STD_UNIT_C4 = 'mF';
  STD_UNIT_C5 = ' F';

  STD_UNIT_L1 = 'nH';
  STD_UNIT_L2 = 'µH';
  STD_UNIT_L3 = 'mH';
  STD_UNIT_L4 = 'kH';
  STD_UNIT_L5 = ' H';

  STD_UNIT_V1 = 'mV';
  STD_UNIT_V2 = ' V';

  STD_UNIT_F1 = 'kHz';
  STD_UNIT_F2 = 'MHz';
  STD_UNIT_F3 = ' Hz';

  STD_UNIT_B1 = 'bps';

  STD_UNIT_D1 = '%';

  STD_UNIT_T1 = 'ns';
  STD_UNIT_T2 = 'µs';
  STD_UNIT_T3 = 'ms';
  STD_UNIT_T4 = ' s';


  { Cyrillic units }

  CYR_UNIT_R1 = 'мОм';
  CYR_UNIT_R2 = 'кОм';
  CYR_UNIT_R3 = 'МОм';
  CYR_UNIT_R4 = 'ГОм';
  CYR_UNIT_R5 = ' Ом';

  CYR_UNIT_C1 = ' пФ ';
  CYR_UNIT_C2 = ' нФ ';
  CYR_UNIT_C3 = 'мкФ ';
  CYR_UNIT_C4 = ' мФ ';
  CYR_UNIT_C5 = '  Ф ';

  CYR_UNIT_L1 = ' нГн';
  CYR_UNIT_L2 = 'мкГн';
  CYR_UNIT_L3 = ' мГн';
  CYR_UNIT_L4 = ' кГн';
  CYR_UNIT_L5 = '  Гн';

  CYR_UNIT_V1 = 'мВ';
  CYR_UNIT_V2 = ' В';

  CYR_UNIT_F1 = 'кГц';
  CYR_UNIT_F2 = 'МГц';
  CYR_UNIT_F3 = ' Гц';

  CYR_UNIT_B1 = 'бод/с';

  CYR_UNIT_D1 = '%';

  CYR_UNIT_T1 = ' нс';
  CYR_UNIT_T2 = 'мкс';
  CYR_UNIT_T3 = ' мс';
  CYR_UNIT_T4 = '  с';


  { Arrays }

  DEF_UNITS: array[0..25] of String = (
    DEF_UNIT_R1, DEF_UNIT_R2, DEF_UNIT_R3, DEF_UNIT_R4, DEF_UNIT_R5,
    DEF_UNIT_C1, DEF_UNIT_C2, DEF_UNIT_C3, DEF_UNIT_C4, DEF_UNIT_C5,
    DEF_UNIT_L1, DEF_UNIT_L2, DEF_UNIT_L3, DEF_UNIT_L4, DEF_UNIT_L5,
    DEF_UNIT_V1, DEF_UNIT_V2,
    DEF_UNIT_F1, DEF_UNIT_F2, DEF_UNIT_F3,
    DEF_UNIT_B1,
    DEF_UNIT_D1,
    DEF_UNIT_T1, DEF_UNIT_T2, DEF_UNIT_T3, DEF_UNIT_T4);

  STD_UNITS: array[0..25] of String = (
    STD_UNIT_R1, STD_UNIT_R2, STD_UNIT_R3, STD_UNIT_R4, STD_UNIT_R5,
    STD_UNIT_C1, STD_UNIT_C2, STD_UNIT_C3, STD_UNIT_C4, STD_UNIT_C5,
    STD_UNIT_L1, STD_UNIT_L2, STD_UNIT_L3, STD_UNIT_L4, STD_UNIT_L5,
    STD_UNIT_V1, STD_UNIT_V2,
    STD_UNIT_F1, STD_UNIT_F2, STD_UNIT_F3,
    STD_UNIT_B1,
    STD_UNIT_D1,
    STD_UNIT_T1, STD_UNIT_T2, STD_UNIT_T3, STD_UNIT_T4);

  CYR_UNITS: array[0..25] of String = (
    CYR_UNIT_R1, CYR_UNIT_R2, CYR_UNIT_R3, CYR_UNIT_R4, CYR_UNIT_R5,
    CYR_UNIT_C1, CYR_UNIT_C2, CYR_UNIT_C3, CYR_UNIT_C4, CYR_UNIT_C5,
    CYR_UNIT_L1, CYR_UNIT_L2, CYR_UNIT_L3, CYR_UNIT_L4, CYR_UNIT_L5,
    CYR_UNIT_V1, CYR_UNIT_V2,
    CYR_UNIT_F1, CYR_UNIT_F2, CYR_UNIT_F3,
    CYR_UNIT_B1,
    CYR_UNIT_D1,
    CYR_UNIT_T1, CYR_UNIT_T2, CYR_UNIT_T3, CYR_UNIT_T4);


// replace raw units to app defined
function UnitsReplace(AInput: String; ASpace: Boolean = False; AType: TTweezersUnits = tuOriginal): String;


implementation

function UnitsReplace(AInput: String; ASpace: Boolean; AType: TTweezersUnits): String;
  var
    i: Integer;
    s: String = '';
  begin
    Result := AInput;

    if ASpace then s := ' ';

    for i := 0 to High(DEF_UNITS) do
      if Result.EndsWith(DEF_UNITS[i]) then
        case AType of
          tuOriginal: Exit(Result.Replace(DEF_UNITS[i], s + DEF_UNITS[i]));
          tuStandard: Exit(Result.Replace(DEF_UNITS[i], s + STD_UNITS[i]));
          tuCyrillic: Exit(Result.Replace(DEF_UNITS[i], s + CYR_UNITS[i]));
          tuLocalize: Exit(Result.Replace(DEF_UNITS[i], s + LOC_UNITS[i]));
          end;
  end;

end.
