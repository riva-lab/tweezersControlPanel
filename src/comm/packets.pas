unit packets;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, modelTypes;

const

  { Magic for autodetecting target device }

  BM8232_MAGIC: TStringArray = ('-> BM8232 Tweezers.', 'Bui=', 'Uave=', 'G=sin', 'G=pwm', 'G=Serial', 'G=Noise');


  { Version prefix }

  BM8232_HWVER = 'BM8232 v';


  { Requests to device }

  BM8232_RQS_HELP = 'help';
  BM8232_RQS_CNTR = 'res';
  BM8232_RQS_INC1 = '+';
  BM8232_RQS_DEC1 = '-';
  BM8232_RQS_CLGO = '+'; // calibration continue

  BM8232_RQS_MODE: array[TTweezersMode] of String      = ('rlc', 'ufd', 'ufd', 'gen');
  BM8232_RQS_FREQ: array[TTweezersMeasFreq] of String  = ('fall', 'f95', 'f1k', 'f10k', 'f95k', 'f160k');
  BM8232_RQS_EQCR: array[TTweezersEqCircuit] of String = ('auto', 'par', 'ser');
  BM8232_RQS_RELM: array[TTweezersRelative] of String  = ('abs', 'rel');
  BM8232_RQS_OFFS: array[TTweezersOffset] of String    = ('b0', 'b6', 'b26');
  BM8232_RQS_GENM: array[TTweezersGenMode] of String   = ('sin003', 'sin03', 'sin3', 'pwm50', 'pwm2k', 'pwm50k', 'uart', 'noise');
  BM8232_RQS_BAND: array[TTweezersBand] of String      = ('bn=0', 'bn=1', 'bn=2', 'bn=3');
  BM8232_RQS_CALI: array[TTweezersCalType] of String   = ('wl', 'wh', 'set0', 'ref', 'wz', 'cals', 'call', '');


  { Messages from device }

  BM8232_MSG_VARR = 'R';
  BM8232_MSG_VARL = 'L';
  BM8232_MSG_VARC = 'C';
  BM8232_MSG_VARZ = 'Z';
  BM8232_MSG_VARQ = 'Q';
  BM8232_MSG_VARP = 'tg';
  BM8232_MSG_MFRQ = 'F';
  BM8232_MSG_EQCR = 'Eq';
  BM8232_MSG_EQPR = 'Par';
  BM8232_MSG_EQSR = 'Ser';
  BM8232_MSG_VARF = 'f';
  BM8232_MSG_VART = 'T';
  BM8232_MSG_VARD = 'D';
  BM8232_MSG_VARN = 'N';
  //BM8232_MSG_BAND = 'Band';
  BM8232_MSG_AREL = 'Rel';
  BM8232_MSG_AABS = 'Abs';
  BM8232_MSG_URMS = 'Urms';
  BM8232_MSG_UAVG = 'Uave';
  BM8232_MSG_UP2P = 'Up-p';
  BM8232_MSG_BIAS = 'Bias';
  BM8232_MSG_GENM = 'G';
  BM8232_MSG_GENF = 'Frequency';
  BM8232_MSG_GEND = 'DutyCycle';
  BM8232_MSG_GENB = 'UART_8N1';
  BM8232_MSG_GENA = 'Amplitude';

  BM8232_MSG_VBND = 'Bui';
  BM8232_MSG_RC12 = 'Rcal12';
  BM8232_MSG_RC03 = 'Rcal3';
  BM8232_MSG_XTAL = 'Fz';
  BM8232_MSG_CALB = 'BiasU';
  BM8232_MSG_CALU = 'CalU';

  BM8232_MSG_CAL0 = 'Open probes';
  BM8232_MSG_CAL1 = 'Close probes';
  BM8232_MSG_CAL2 = 'Connect Rcal12';
  BM8232_MSG_CAL3 = 'Connect Rcal3';
  BM8232_MSG_CAL4 = 'Bnd';
  BM8232_MSG_CAL5 = 'Complete';

  BM8232_MSGV_FREQ: array[TTweezersMeasFreq] of String  = ('-', '95Hz', '1kHz', '10kHz', '95kHz', '160kHz');
  BM8232_MSGV_GENM: array[TTweezersGenMode] of String   = ('sin30mV', 'sin300mV', 'sin3V', 'pwm50Hz', 'pwm2kHz', 'pwm50kHz', 'Serial', 'Noise');
  BM8232_MSGV_EQCR: array[TTweezersEqCircuit] of String = ('???', 'Parallel', 'Serial');



type

  TMessageProc = procedure(ID, DefVal: String) of object;

  TProtocolItem = record
    ID, Def: String;
    VStr:    PString;
    VInt:    PInteger;
    Vals:    TStringArray;
    OnMsg:   TMessageProc;
  end;

  { TMessagesParser }

  TMessagesParser = class
  private
    FEntries:   array of TProtocolItem;
    FFWVersion: String;

    procedure SetInput(AValue: String);
    procedure UpdateEntry(AID, AValue: String);
    procedure GetFWVersion(AValue: String);

  public
    constructor Create;
    destructor Destroy; override;

    procedure AddMessage(AID: String; ADest: PString; ADefault: String = ''; OnMessage: TMessageProc = nil);
    procedure AddMessage(AID: String; ADest: PString; ADestInt: PInteger; APossibleValues: TStringArray);

    property Input: String write SetInput;
    property FWVersion: String read FFWVersion;
  end;


// create valid packet ready to send
function RequestCreate(ACommand: String; AData: String = ''; ARepeat: Integer = 1): String;

// get id-value pair from input string, return tail of string
function PeakValue(AInput: String; var AID, AValue: String): String;


implementation


{ TMessagesParser }

constructor TMessagesParser.Create;
  begin
    SetLength(FEntries, 0);
    FFWVersion := '';
  end;

destructor TMessagesParser.Destroy;
  begin
    SetLength(FEntries, 0);
    inherited Destroy;
  end;

procedure TMessagesParser.AddMessage(AID: String; ADest: PString; ADefault: String; OnMessage: TMessageProc);
  begin
    SetLength(FEntries, Length(FEntries) + 1);

    with FEntries[High(FEntries)] do
      begin
      ID    := AID;
      Def   := ADefault;
      VStr  := ADest;
      VInt  := nil;
      OnMsg := OnMessage;
      end;
  end;

procedure TMessagesParser.AddMessage(AID: String; ADest: PString; ADestInt: PInteger; APossibleValues: TStringArray);
  begin
    SetLength(FEntries, Length(FEntries) + 1);

    with FEntries[High(FEntries)] do
      begin
      ID    := AID;
      VStr  := ADest;
      VInt  := ADestInt;
      Vals  := APossibleValues;
      OnMsg := nil;
      end;
  end;

procedure TMessagesParser.SetInput(AValue: String);
  var
    _id, _value: String;
    fuze:        Integer = 32;

  procedure TransformValue;
    begin
      _value := _value.Replace(#174, 'Ω');

      if _id.IndexOfAny([BM8232_MSG_VARL]) = 0 then
        if _value.Contains('---') then _value := '*';

      if _id.IndexOfAny([BM8232_MSG_VARR, BM8232_MSG_VARZ]) = 0 then
        if _value.Contains('---') then _value := '∞';
    end;
  begin
    GetFWVersion(AValue);
    while (AValue <> '') or (fuze = 0) do
      begin
      AValue := PeakValue(AValue, _id, _value);

      TransformValue;
      UpdateEntry(_id, _value);

      fuze -= 1;
      end;
  end;

procedure TMessagesParser.UpdateEntry(AID, AValue: String);
  var
    i, j: Integer;
  begin
    if Length(FEntries) = 0 then Exit;

    for i := 0 to High(FEntries) do
      with FEntries[i] do
        if ID = AID then
          begin

          if Assigned(VInt) then
            begin
            Integer(VInt^) := -1;

            for j := 0 to High(Vals) do
              if Vals[j] = AValue then
                begin
                Integer(VInt^) := j;
                Break;
                end;
            end
          else
          if AValue = '' then AValue := Def;

          if Assigned(VStr) then String(VStr^) := AValue;
          if Assigned(OnMsg) then OnMsg(ID, Def);
          Break;
          end;
  end;

procedure TMessagesParser.GetFWVersion(AValue: String);
  var
    i: SizeInt;
  begin
    i := AValue.IndexOf(BM8232_HWVER);
    if i < 0 then Exit;
    i += Length(BM8232_HWVER) + 1;

    FFWVersion := '';
    while (i < Length(AValue)) and (AValue[i] in ['0'..'9']) do
      begin
      FFWVersion += AValue[i];
      Inc(i);
      end;
  end;


{ Functions }

function RequestCreate(ACommand: String; AData: String; ARepeat: Integer): String;
  begin
    Result := '';

    while ARepeat > 0 do
      begin
      Result  += ACommand;
      ARepeat -= 1;
      end;

    if AData <> '' then
      Result += '=' + AData;

    Result += #13;
  end;

function PeakValue(AInput: String; var AID, AValue: String): String;
  var
    _arr: TStringArray;
  begin
    AID    := '';
    AValue := '';
    Result := AInput.Trim([' ', ',']).Replace(#13, ',').Replace(#10, ',');
    if Result = '' then Exit;

    _arr := Result.Split(',');
    if Length(_arr) = 0 then Exit;

    Result := Result.Remove(0, Length(_arr[0]));
    _arr   := _arr[0].Trim([' ', '.']).Split('=');

    if Length(_arr) > 0 then AID    := _arr[0];
    if Length(_arr) > 1 then AValue := _arr[1];
  end;


end.
