unit model;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, modelTypes, comm, packets, deviceUnits;


type
  TTweezersDataSystem = record
    FWmajor, FWminor, Rcal12, Rcal3, Fz, BiasU, CalU: String;
    CalStage: TTweezersCalStage;
  end;

  TTweezersDataRLC = record
    R, CL, Z, QT, E, MF, BUI: String;
    Item:  TTweezersItem;
    EqCct: TTweezersEqCircuit;
  end;

  TTweezersDataVoltmeter = record
    RMS, avg, pp: String;
  end;

  TTweezersDataFreqMeter = record
    F, T, D, N: String;
  end;

  TTweezersDataGenerator = record
    Mode, Value: String;
  end;


  { TTweezers }

  TTweezers  = class
  const
    MODEL_ID = 'BM8232 Tweezers';

  private
    FComm:     TSerialComm;
    FParser:   TMessagesParser;
    FInit:     Boolean;
    FGetInfo:  Boolean;
    FPort:     String;
    FMode:     TTweezersMode;
    FRelative: TTweezersRelative;
    FMeasF:    TTweezersMeasFreq;
    FBand:     TTweezersBand;
    FEq:       TTweezersEqCircuit;
    FOffset:   TTweezersOffset;
    FGen:      TTweezersGenMode;
    FStatus:   TTweezersStatus;
    FCalType:  TTweezersCalType;
    FFreqIdx:  Integer;
    FGenIdx:   Integer;

    procedure DeviceOnConnect;
    procedure DeviceOnDisconnect;
    procedure DeviceOnMessage;
    procedure DeviceOnCalStage(ID, DefVal: String);

    function GetStatus: TTweezersStatus;
    function GetPort: String;

    procedure SetPort(AValue: String);
    procedure SetMode(AValue: TTweezersMode);
    procedure SetMeasF(AValue: TTweezersMeasFreq);
    procedure SetBand(AValue: TTweezersBand);
    procedure SetEq(AValue: TTweezersEqCircuit);
    procedure SetOffset(AValue: TTweezersOffset);
    procedure SetGen(AValue: TTweezersGenMode);
    procedure SetRelative(AValue: TTweezersRelative);

    procedure Request(ACommand: String; AData: String = ''; ARepeat: Integer = 1);
    procedure RequestSystemInfo;
    procedure ParseSystemInfo;

  public
    RLC:  array[TTweezersMeasFreq] of TTweezersDataRLC;
    V:    TTweezersDataVoltmeter;
    F:    TTweezersDataFreqMeter;
    G:    TTweezersDataGenerator;
    Info: TTweezersDataSystem;

    OnUpdate:     TNotifyEvent;
    OnConnect:    TNotifyEvent;
    OnDisconnect: TNotifyEvent;
    OnInfo:       TNotifyEvent;
    OnCalibrate:  TNotifyEvent;

    constructor Create;
    destructor Destroy; override;

    procedure Calibrate(ACalType: TTweezersCalType; AValue: DWord);
    procedure CalibrationContinue;
    procedure GenParameterChange(ASteps: Integer);
    procedure ResetCounter;

    property Port: String read GetPort write SetPort;
    property Status: TTweezersStatus read GetStatus;
    property Mode: TTweezersMode read FMode write SetMode;
    property Relative: TTweezersRelative read FRelative write SetRelative;
    property MeasF: TTweezersMeasFreq read FMeasF write SetMeasF;
    property Band: TTweezersBand read FBand write SetBand;
    property Eq: TTweezersEqCircuit read FEq write SetEq;
    property Offset: TTweezersOffset read FOffset write SetOffset;
    property Gen: TTweezersGenMode read FGen write SetGen;
  end;


var
  tweezers: TTweezers;


implementation


{ TTweezers }

constructor TTweezers.Create;
  begin
    FComm              := TSerialComm.Create;
    FComm.Magic        := BM8232_MAGIC;
    FComm.OnConnect    := @DeviceOnConnect;
    FComm.OnDisconnect := @DeviceOnDisconnect;
    FComm.OnMessage    := @DeviceOnMessage;

    FParser := TMessagesParser.Create;
    with FParser do
      begin
      AddMessage(BM8232_MSG_VARR, @RLC[tfAuto].R);
      AddMessage(BM8232_MSG_VARC, @RLC[tfAuto].CL);
      AddMessage(BM8232_MSG_VARL, @RLC[tfAuto].CL);
      AddMessage(BM8232_MSG_VARQ, @RLC[tfAuto].QT);
      AddMessage(BM8232_MSG_VARP, @RLC[tfAuto].QT);
      AddMessage(BM8232_MSG_VARZ, @RLC[tfAuto].Z);
      AddMessage(BM8232_MSG_VBND, @RLC[tfAuto].BUI);
      AddMessage(BM8232_MSG_EQPR, @RLC[tfAuto].E, 'Parallel');
      AddMessage(BM8232_MSG_EQSR, @RLC[tfAuto].E, 'Serial');
      AddMessage(BM8232_MSG_MFRQ, @RLC[tfAuto].MF, @FFreqIdx, BM8232_MSGV_FREQ);

      AddMessage(BM8232_MSG_VARF, @F.F);
      AddMessage(BM8232_MSG_VART, @F.T);
      AddMessage(BM8232_MSG_VARD, @F.D);
      AddMessage(BM8232_MSG_VARN, @F.N);

      AddMessage(BM8232_MSG_URMS, @V.RMS);
      AddMessage(BM8232_MSG_UAVG, @V.avg);
      AddMessage(BM8232_MSG_UP2P, @V.pp);

      AddMessage(BM8232_MSG_GENM, @G.Mode, @FGenIdx, BM8232_MSGV_GENM);
      AddMessage(BM8232_MSG_GENF, @G.Value, BM8232_MSG_GENF);
      AddMessage(BM8232_MSG_GENA, @G.Value, BM8232_MSG_GENA);
      AddMessage(BM8232_MSG_GENB, @G.Value, BM8232_MSG_GENB);
      AddMessage(BM8232_MSG_GEND, @G.Value, BM8232_MSG_GEND);

      AddMessage(BM8232_MSG_RC12, @Info.Rcal12);
      AddMessage(BM8232_MSG_RC03, @Info.Rcal3);
      AddMessage(BM8232_MSG_CALB, @Info.BiasU);
      AddMessage(BM8232_MSG_CALU, @Info.CalU);
      AddMessage(BM8232_MSG_XTAL, @Info.Fz);

      AddMessage(BM8232_MSG_CAL0, nil, '1', @DeviceOnCalStage);
      AddMessage(BM8232_MSG_CAL1, nil, '2', @DeviceOnCalStage);
      AddMessage(BM8232_MSG_CAL2, nil, '3', @DeviceOnCalStage);
      AddMessage(BM8232_MSG_CAL3, nil, '4', @DeviceOnCalStage);
      AddMessage(BM8232_MSG_CAL4, nil, '5', @DeviceOnCalStage);
      AddMessage(BM8232_MSG_CAL5, nil, '6', @DeviceOnCalStage);
      end;

    FPort        := '';
    FInit        := True;
    FCalType     := tcNone;
    FMode        := tmRLC;
    FRelative    := False;
    FMeasF       := tfAuto;
    FBand        := tbAuto;
    FEq          := teAuto;
    FOffset      := toDisable;
    FGen         := tgSine30;
    FStatus      := tsOffline;
    OnConnect    := nil;
    OnDisconnect := nil;
    OnUpdate     := nil;
    OnInfo       := nil;
    OnCalibrate  := nil;
  end;

destructor TTweezers.Destroy;
  begin
    FComm.Free;
    FParser.Free;
    inherited Destroy;
  end;

procedure TTweezers.Calibrate(ACalType: TTweezersCalType; AValue: DWord);
  begin
    Info.CalStage := tcsStart;
    FCalType      := ACalType;

    case FCalType of

      tcOS, tcOSL:
        begin
        if FStatus <> tsOnline then Info.CalStage := tcsError;
        if Assigned(OnCalibrate) then OnCalibrate(Self);
        Info.CalStage := tcsOpenTips;
        end;

      tcRes12, tcRes3, tcUhigh, tcXtal:
        Request(BM8232_RQS_CALI[FCalType], AValue.ToString);

      tcUlow:
        Request(BM8232_RQS_CALI[FCalType]);
      end;
  end;

procedure TTweezers.CalibrationContinue;
  begin
    if FCalType in [tcOS, tcOSL] then
      if Info.CalStage = tcsStart then
        Request(BM8232_RQS_CALI[FCalType])
      else
        Request(BM8232_RQS_CLGO);
  end;

procedure TTweezers.DeviceOnConnect;
  begin
    RequestSystemInfo;

    FCalType := tcNone;
    Mode     := FMode;
    FFreqIdx := -1;

    if Assigned(OnConnect) then OnConnect(Self);
  end;

procedure TTweezers.DeviceOnDisconnect;
  begin
    FInit         := True;
    Info.CalStage := tcsError;
    Info.FWmajor  := '';
    Info.FWminor  := '';

    if Assigned(OnCalibrate) and (FCalType <> tcNone) then OnCalibrate(Self);
    if Assigned(OnDisconnect) then OnDisconnect(Self);

    FCalType := tcNone;
  end;

procedure TTweezers.DeviceOnMessage;
  var
    eqc: TTweezersEqCircuit;
  begin
    FParser.Input := FComm.Message;
    ParseSystemInfo;

    if Mode = tmRLC then
      with RLC[tfAuto] do
        begin
        Item := tiUnknown;
        if CL.Contains(DEF_UNIT_C5) then Item := tiCapacitor;
        if CL.Contains(DEF_UNIT_L5) then Item := tiInductor;

        for eqc in TTweezersEqCircuit do
          if E.Contains(BM8232_MSGV_EQCR[eqc]) then
            EqCct := eqc;

        if TTweezersMeasFreq(FFreqIdx) in [tf95..tf160k] then
          begin
          RLC[TTweezersMeasFreq(FFreqIdx)] := RLC[tfAuto];

          if Assigned(OnUpdate) then OnUpdate(Self);
          end;
        end
    else
    if Assigned(OnUpdate) then OnUpdate(Self);
  end;

procedure TTweezers.DeviceOnCalStage(ID, DefVal: String);
  begin
    if Info.CalStage = tcsDone then Exit;

    if FCalType in [tcOS, tcOSL] then
      Info.CalStage := TTweezersCalStage(StrToIntDef(DefVal, 0))
    else
      Info.CalStage := tcsError;

    if Assigned(OnCalibrate) then OnCalibrate(Self);
  end;

function TTweezers.GetStatus: TTweezersStatus;
  begin
    Result := FStatus;

    if FComm.Connected then FStatus := tsOnline else
    if FComm.Autoconnect then FStatus := tsScanning else
      FStatus := tsOffline;

    Result := FStatus;
  end;

function TTweezers.GetPort: String;
  begin
    Result := FComm.Port;
  end;

procedure TTweezers.SetPort(AValue: String);
  begin
    FPort             := AValue;
    FComm.Port        := FPort;
    FComm.Autoconnect := not FPort.StartsWith('COM') and not FPort.IsEmpty;
  end;

procedure TTweezers.SetEq(AValue: TTweezersEqCircuit);
  begin
    if not FInit and (FEq = AValue) then Exit;
    FEq := AValue;
    Request(BM8232_RQS_EQCR[FEq]);
  end;

procedure TTweezers.SetGen(AValue: TTweezersGenMode);
  begin
    if not FInit and (FGen = AValue) then Exit;
    FGen := AValue;
    Request(BM8232_RQS_GENM[FGen]);
  end;

procedure TTweezers.SetRelative(AValue: TTweezersRelative);
  begin
    if not FInit and (FRelative = AValue) then Exit;
    FRelative := AValue;
    Request(BM8232_RQS_RELM[FRelative]);
  end;

procedure TTweezers.SetMeasF(AValue: TTweezersMeasFreq);
  begin
    if not FInit and (FMeasF = AValue) then Exit;
    FMeasF := AValue;
    Request(BM8232_RQS_FREQ[FMeasF]);
  end;

procedure TTweezers.SetMode(AValue: TTweezersMode);
  begin
    //if not FInit and (FMode = AValue) then Exit;
    FMode := AValue;
    Request(BM8232_RQS_MODE[FMode]);

    FInit    := True;
    Relative := FRelative;
    MeasF    := FMeasF;
    Eq       := FEq;
    Band     := FBand;
    Offset   := FOffset;
    Gen      := FGen;
    FInit    := False;
  end;

procedure TTweezers.SetOffset(AValue: TTweezersOffset);
  begin
    if not FInit and (FOffset = AValue) then Exit;
    FOffset := AValue;
    Request(BM8232_RQS_OFFS[FOffset]);
  end;

procedure TTweezers.SetBand(AValue: TTweezersBand);
  begin
    if not FInit and (FBand = AValue) then Exit;
    FBand := AValue;
    Request(BM8232_RQS_BAND[FBand]);
  end;

procedure TTweezers.GenParameterChange(ASteps: Integer);
  begin
    if ASteps = 0 then Exit;
    if ASteps > 0 then
      Request(BM8232_RQS_INC1, '', Abs(ASteps)) else
      Request(BM8232_RQS_DEC1, '', Abs(ASteps));
  end;

procedure TTweezers.ResetCounter;
  begin
    Request(BM8232_RQS_CNTR);
  end;

procedure TTweezers.Request(ACommand: String; AData: String; ARepeat: Integer);
  begin
    FComm.Request := RequestCreate(ACommand, AData, ARepeat);
  end;

procedure TTweezers.RequestSystemInfo;
  begin
    Info.Rcal12 := '';
    Info.Rcal3  := '';
    Info.Fz     := '';
    FGetInfo    := True;

    Request(BM8232_RQS_MODE[tmRLC]);
    Request(BM8232_RQS_CALI[tcRes12], '0');
    Request(BM8232_RQS_CALI[tcRes3], '0');
    Request(BM8232_RQS_MODE[tmVoltmeter]);
    Request(BM8232_RQS_CALI[tcXtal], '0');
    Request(BM8232_RQS_HELP);
  end;

procedure TTweezers.ParseSystemInfo;

  procedure TransformValue(var S: String; AUnit: String);
    begin
      S := S.Replace(AUnit, '').Replace('.', '').Trim;
    end;

  function AllNotEmpty(SA: TStringArray): Boolean;
    var
      s: String;
    begin
      Result := True;
      for s in SA do
        if s = '' then Exit(False);
    end;
  begin
    Info.FWmajor := FParser.FWVersion;

    with Info do
      if FGetInfo and AllNotEmpty([Rcal12, Rcal3, Fz]) then
        begin
        FGetInfo := False;
        TransformValue(Rcal12, DEF_UNIT_R5);
        TransformValue(Rcal3, DEF_UNIT_R5);
        TransformValue(Fz, DEF_UNIT_F3);
        if Assigned(OnInfo) then OnInfo(Self);
        end;
  end;



initialization
  tweezers := TTweezers.Create;

end.
