program _tweezersCP;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,

  // project forms
  fm_main, fm_calibration, fm_confirm,

  // for theme (AppTuner) support
  SysUtils, LazUTF8, AppTuner, config_record;

  {$R *.res}

begin
  { CRITICAL! Load INI file as soon as possible to support dark theme.
    INI file should be loaded before Application.Initialize method! }
  appTunerEx.IniFile := ExtractFilePath(ParamStrUTF8(0)) + SETTINGS_FILE;

  RequireDerivedFormResource := True;
  Application.Title := 'tweezersControlPanel';
  Application.Scaled := True;
  Application.Initialize;
  Application.CreateForm(TfmMain, fmMain);
  Application.CreateForm(TfmCalibration, fmCalibration);    
  Application.CreateForm(TfmConfirm, fmConfirm);
  Application.Run;
end.
