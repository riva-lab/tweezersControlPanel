unit config_record;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, AppSettings;


type

  { Project settings.
    Saved and restored from INI file by TAppSettings class.
  }
  TAppConfig = record

    app: record
      lang:     Integer;
      sticky:   Boolean;
      tooltips: Boolean;
      end;

    val: record
      font:     Integer;
      units:    Integer;
      sepdec:   Integer;
      sepths:   Integer;
      sepunits: Boolean;
      end;

    misc: record
      hold:  Boolean;
      hint:  Boolean;
      table: Boolean;
      end;

    log: record
      enable:   Boolean;
      cont:     Boolean;
      mm:       Integer;
      ss:       Integer;
      filename: String;
      end;

    connect: record
      modego: Boolean;
      end;

    cal: record
      readval:   Boolean;
      rlc:       Boolean;
      autoclose: Boolean;
      uf:        Boolean;
      end;

    work: record
      fmeas:  Integer;
      eq:     Integer;
      offset: Integer;
      gen:    Integer;
      end;
  end;


const
  SETTINGS_FILE = 'settings.ini';


// init user defined setting entries
procedure InitConfigVariables;


var
  Settings: TAppSettings; // class for work with settings
  cfg:      TAppConfig;   // configuration record with project settings


implementation


procedure InitConfigVariables;
  begin
  end;


initialization
  Settings := TAppSettings.Create;

end.
