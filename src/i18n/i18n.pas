
{ This is project localization file.
  It contains all strings which must be translated.
}
unit i18n;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, modelTypes;

resourcestring
  {
    Put your i18n string in this section.
  }

  { Strings }

  TXT_AUTOSCAN    = 'Auto search';
  TXT_STATUS_ON   = 'online';
  TXT_STATUS_OF   = 'offline';
  TXT_STATUS_SC   = 'searching...';
  TXT_VAL_ACTIVE  = 'active';
  TXT_VAL_HOLD    = 'HOLDED';
  TXT_LOG_ADD     = 'LOG +';
  TXT_LOG_BEGIN   = 'LOGGING STARTED';
  TXT_LOG_DOING   = 'Logging: %s';
  TXT_LOG_END     = 'LOGGING FINISHED';

  TXT_GEN_SIN     = 'frequency';
  TXT_GEN_PWM     = 'duty cycle';
  TXT_GEN_SER     = 'serial, 8-N-1, baudrate';
  TXT_GEN_NOI     = 'amplitude';
  TXT_ITEM_C      = 'capacitance, C';
  TXT_ITEM_L      = 'inductance, L';
  TXT_ITEM_Q      = 'quality factor, Q';
  TXT_ITEM_TG     = 'loss tangent, tan';
  TXT_EQ_PAR      = 'Parallel';
  TXT_EQ_SER      = 'Serial';

  TXT_CAL_0       = 'Open tweezers tips while holding it in position in which measurements will normally be taken.';
  TXT_CAL_1       = 'Securely close tweezer tips and hold it stationary.';
  TXT_CAL_2       = 'Securely connect 150 Ohm reference resistor for ranges 1 && 2 to tweezers tips.';
  TXT_CAL_3       = 'Securely connect 20 kOhm reference resistor for range 3 to tweezers tips.';
  TXT_CAL_5       = 'RLC-meter calibration complete. You can close this window.';
  TXT_CAL_6       = 'Something went wrong. Please try calibration again.';

  TXT_WINCAL_OS   = 'RLC "Open-Short" Calibration';
  TXT_WINCAL_OSL  = 'RLC "Open-Short-Load" Calibration';
  TXT_WINCAL_GO   = 'Continue';
  TXT_WINCAL_EXIT = 'Exit';

  TXT_COPIED      = 'Value "%s" has been copied to clipboard.';

  TXT_RST_CAPTION = 'Confirm settings reset';
  TXT_RST_MESSAGE = 'App will be closed and current settings will be reverted to their defaults.' + LineEnding + 'Do you really want to reset it?';

  TXT_UNITSPREF_1 = 'Original';
  TXT_UNITSPREF_2 = 'International';
  TXT_UNITSPREF_3 = 'Cyrillic';
  TXT_UNITSPREF_4 = 'Localized';

  TXT_DECIMAL_1   = 'Dot';
  TXT_DECIMAL_2   = 'Comma';

  TXT_THOUSAND_1  = 'Original';
  TXT_THOUSAND_2  = 'None';
  TXT_THOUSAND_3  = 'Comma';
  TXT_THOUSAND_4  = 'Dot';
  TXT_THOUSAND_5  = 'Space';


  { Units }

  LOC_UNIT_R1 = 'mΩ';
  LOC_UNIT_R2 = 'kΩ';
  LOC_UNIT_R3 = 'MΩ';
  LOC_UNIT_R4 = 'GΩ';
  LOC_UNIT_R5 = ' Ω';

  LOC_UNIT_C1 = 'pF';
  LOC_UNIT_C2 = 'nF';
  LOC_UNIT_C3 = 'uF';
  LOC_UNIT_C4 = 'mF';
  LOC_UNIT_C5 = ' F';

  LOC_UNIT_L1 = 'nH';
  LOC_UNIT_L2 = 'uH';
  LOC_UNIT_L3 = 'mH';
  LOC_UNIT_L4 = 'kH';
  LOC_UNIT_L5 = ' H';

  LOC_UNIT_V1 = 'mV';
  LOC_UNIT_V2 = ' V';

  LOC_UNIT_F1 = 'kHz';
  LOC_UNIT_F2 = 'MHz';
  LOC_UNIT_F3 = ' Hz';

  LOC_UNIT_B1 = 'bps';

  LOC_UNIT_D1 = '%';

  LOC_UNIT_T1 = 'ns';
  LOC_UNIT_T2 = 'us';
  LOC_UNIT_T3 = 'ms';
  LOC_UNIT_T4 = ' s';


  { Keymap }

  KEYMAP_01 = 'Operating mode';
  KEYMAP_02 = 'Generator parameter set';
  KEYMAP_03 = 'Values';
  KEYMAP_04 = 'font size';
  KEYMAP_05 = 'Settings';
  KEYMAP_06 = 'Relative';
  KEYMAP_07 = 'Reset counter';
  KEYMAP_08 = 'On top';
  KEYMAP_09 = 'Connection';
  KEYMAP_10 = 'Disconnect';
  KEYMAP_11 = 'About';
  KEYMAP_12 = 'Hold';
  KEYMAP_13 = 'Logging';
  KEYMAP_14 = 'RLC table row';
  KEYMAP_15 = 'select';                           
  KEYMAP_16 = 'Grouped switches on page control';
  KEYMAP_17 = 'First switch on page';
  KEYMAP_18 = 'control';
  KEYMAP_19 = 'Show this';
  KEYMAP_20 = 'keymap';
  KEYMAP_21 = 'Exit';


const
  {
    Put arrays of i18n strings in this section.
    Note that you must provide specific length of array: array[0..3] of String
    You can also set range using enumerations:           array[TEnumType] of String
    Do not use dynamic arrays! They does not work appropriatelly.
  }

  TXT_STATUS: array[TTweezersStatus] of String = (
    TXT_STATUS_OF, TXT_STATUS_ON, TXT_STATUS_SC);

  TXT_GEN_LABEL: array[TTweezersGenMode] of String = (
    TXT_GEN_SIN, TXT_GEN_SIN, TXT_GEN_SIN, TXT_GEN_PWM, TXT_GEN_PWM, TXT_GEN_PWM, TXT_GEN_SER, TXT_GEN_NOI);

  TXT_LABEL_LC: array[Boolean] of String = (
    TXT_ITEM_L, TXT_ITEM_C);

  TXT_LABEL_Q: array[Boolean] of String = (
    TXT_ITEM_Q, TXT_ITEM_TG);

  TXT_EQ_CIRC: array[TTweezersEqCircuit] of String = (
    '', TXT_EQ_PAR, TXT_EQ_SER);

  TXT_CAL_LABEL: array[TTweezersCalStage] of String = (
    TXT_CAL_0, TXT_CAL_0, TXT_CAL_1, TXT_CAL_2, TXT_CAL_3, '', TXT_CAL_5, TXT_CAL_6);

  TXT_WINCAL_CAP: array[Boolean] of String = (
    TXT_WINCAL_OS, TXT_WINCAL_OSL);

  LOC_UNITS: array[0..25] of String = (
    LOC_UNIT_R1, LOC_UNIT_R2, LOC_UNIT_R3, LOC_UNIT_R4, LOC_UNIT_R5,
    LOC_UNIT_C1, LOC_UNIT_C2, LOC_UNIT_C3, LOC_UNIT_C4, LOC_UNIT_C5,
    LOC_UNIT_L1, LOC_UNIT_L2, LOC_UNIT_L3, LOC_UNIT_L4, LOC_UNIT_L5,
    LOC_UNIT_V1, LOC_UNIT_V2,
    LOC_UNIT_F1, LOC_UNIT_F2, LOC_UNIT_F3,
    LOC_UNIT_B1,
    LOC_UNIT_D1,
    LOC_UNIT_T1, LOC_UNIT_T2, LOC_UNIT_T3, LOC_UNIT_T4);

  ITEMS_UNITSPREF: array[0..3] of String = (
    TXT_UNITSPREF_1, TXT_UNITSPREF_2, TXT_UNITSPREF_3, TXT_UNITSPREF_4);

  ITEMS_DECIMAL: array[0..1] of String = (
    TXT_DECIMAL_1, TXT_DECIMAL_2);

  ITEMS_THOUSAND: array[0..4] of String = (
    TXT_THOUSAND_1, TXT_THOUSAND_2, TXT_THOUSAND_3, TXT_THOUSAND_4, TXT_THOUSAND_5);

  KEYMAP_LABELS: array[0..20] of String = (
    KEYMAP_01, KEYMAP_02, KEYMAP_03, KEYMAP_04, KEYMAP_05,
    KEYMAP_06, KEYMAP_07, KEYMAP_08, KEYMAP_09, KEYMAP_10,
    KEYMAP_11, KEYMAP_12, KEYMAP_13, KEYMAP_14, KEYMAP_15,
    KEYMAP_16, KEYMAP_17, KEYMAP_18, KEYMAP_19, KEYMAP_20,
    KEYMAP_21);

var
  KEYMAP_LABELS_ORIG: array of String;

implementation

initialization
  KEYMAP_LABELS_ORIG := KEYMAP_LABELS;

end.
