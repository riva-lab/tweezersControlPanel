unit fm_main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ActnList, ComCtrls,
  ToolWin, ExtCtrls, StdCtrls, Spin, Menus, Buttons, Grids, SpinEx,
  LCLType, LazUTF8, LCLIntf, Math, Clipbrd, EditBtn, Types,
  ImageSVGList, BCSVGViewer, BGRABitmap,

  // service units
  SerialPortUtils, UTF8Logger, u_helpers, resHelper, appAbout, AppTuner,

  // app internationalization
  AppLocalizer, i18n,

  // app settings
  AppSettings, config_record,

  // hardware units
  model, modelTypes, deviceUnits, deviceNumbers, packets,

  // forms
  fm_calibration, fm_confirm;


const
  VAL_STATUS_COLOR: array[TTweezersStatus] of TColor = (
    $BBBBFF, $BBFFBB, $BBFFFF);

  VAL_STATUS_COLOR_DARK: array[TTweezersStatus] of TColor = (
    $4444AA, $448844, $449999);

  LOG_FILE_DEF  = 'tweezers.log'; // log default filename
  FONT_STD      = 'Consolas';     // values default font

  FONT_SIZE_MIN = 16;   // values font size min
  FONT_SIZE_MAX = 160;  // values font size max

  TMR_LOGGED    = 4;    // ticks before 'LOG+' hint hiding
  TMR_HOLDED    = 1000; // [ms] HOLD banner blink time

  IMG_CAP_ID    = 13;   // capacitor icon index in imSVGList
  IMG_IND_ID    = 14;   // inductance icon index in imSVGList


type

  { TfmMain }

  TfmMain = class(TForm)

    // controls are in separate file to tidy up code
    {$Include fm_main_controls.inc}

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

    procedure actionCommandExecute(Sender: TObject);
    procedure actionCalibrateExecute(Sender: TObject);
    procedure actionExecute(Sender: TObject);
    procedure linkActionExecute(Sender: TObject);

    procedure tmrTimerTimer(Sender: TObject);

    procedure lsPortKeyPress(Sender: TObject; var Key: char);
    procedure lsPortSelectionChange(Sender: TObject; User: Boolean);
    procedure lsPortDrawItem(Control: TWinControl; Index: Integer; ARect: TRect; State: TOwnerDrawState);
    procedure sgRLCAfterSelection(Sender: TObject; aCol, aRow: Integer);
    procedure sgRLCDrawCell(Sender: TObject; aCol, aRow: Integer; aRect: TRect; aState: TGridDrawState);
    procedure sgRLCKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure sgRLCPrepareCanvas(Sender: TObject; aCol, aRow: Integer; aState: TGridDrawState);
    procedure stbStatusResize(Sender: TObject);
    procedure tiTrayClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);

  private
    procedure OnTweezersConnect(Sender: TObject);
    procedure OnTweezersDisconnect(Sender: TObject);
    procedure OnTweezersUpdate(Sender: TObject);
    procedure OnTweezersInfo(Sender: TObject);
    procedure OnTweezersCalibrate(Sender: TObject);

    function FormatNumber(S: String): String;

    procedure ActionsEnable(AActions: array of TAction; AEnable: Boolean);
    procedure AppOnKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure OnValueClick(Sender: TObject);
    procedure OnLinkAboutClick(Sender: TObject);
    procedure ShowModePage(AMode: TTweezersMode);
    procedure ShowLogBanner(ASingle, AEnd: Boolean);

    procedure TableSelectRow;
    procedure SelectPage(AIndex: Integer);
    procedure UpdatePortsList(Sender: TObject);
    procedure UpdateStatus;
    procedure UpdateLog;
    procedure UpdateBanners;
    procedure UpdateShortcuts;
    procedure UpdateAbout;

    procedure InitConfigComponents;
    procedure InitPages;
    procedure InitSwitches;
    procedure SaveSwitches;
    procedure InitLocalizer;

    procedure OnLangChange(Sender: TObject);

    procedure AdjustTheme;
    procedure AdjustSizes;
  public

  end;


var
  fmMain:       TfmMain;
  logger:       TUTF8Logger;
  logHeader:    Boolean = False;
  logTimeout:   TDateTime;
  fontSize:     Integer;
  prevRow:      Integer = -1;
  appFmtSet:    TFormatSettings;
  statusColors: array[TTweezersStatus] of TColor;


implementation

{$R *.lfm}


{ TfmMain }

procedure TfmMain.FormCreate(Sender: TObject);
  begin
    UpdateAbout;
    InitPages;

    Application.AddOnKeyDownHandler(@AppOnKeyDown, True);

    Settings.IniFile := ExtractFilePath(ParamStrUTF8(0)) + SETTINGS_FILE;

    InitLocalizer;
    appLocalizerEx.OnLanguageChange := @OnLangChange;
    appLocalizerEx.Load(
      Format('%0:slang%0:slanguages.ini', [DirectorySeparator]),
      Format('%0:slang%0:stweezersControlPanel', [DirectorySeparator]));

    // load languages list
    cbLanguage.Items.SetStrings(appLocalizerEx.Languages);
    cbLanguage.ItemIndex := 0;
    cbLanguage.OnChange  := @actionExecute;

    // load list of available fonts
    cbFonts.Clear;
    cbFonts.Items.AddStrings(Screen.Fonts);
    cbFonts.Text := FONT_STD;

    fontSize := Scale96ToScreen(32);

    tweezers.OnConnect    := @OnTweezersConnect;
    tweezers.OnDisconnect := @OnTweezersDisconnect;
    tweezers.OnUpdate     := @OnTweezersUpdate;
    tweezers.OnInfo       := @OnTweezersInfo;
    tweezers.OnCalibrate  := @OnTweezersCalibrate;

    logger            := TUTF8Logger.Create();
    logger.AppendMode := True;

    SerialEnumerator.OnUpdate     := @UpdatePortsList;
    SerialEnumerator.Interval     := 200;
    SerialEnumerator.CheckForBusy := True;
    SerialEnumerator.Update       := True;
    SerialEnumerator.UpdateNow;

    dlgFileLog.FileName := ExtractFilePath(ParamStrUTF8(0)) + LOG_FILE_DEF;

    appFmtSet                := FormatSettings;
    appFmtSet.LongTimeFormat := 'hh:nn:ss';
  end;

procedure TfmMain.FormShow(Sender: TObject);
  begin
    OnShow := nil;

    // init app tuner
    appTunerEx.AddAllForms;

    InitConfigComponents;
    InitConfigVariables;

    Settings.SyncValues;
    Settings.Load;
    Settings.SyncComponents;
    actionExecute(cbLanguage);
    actionExecute(cbSticky);

    AdjustSizes;

    // use this for autosize and autoset constraints
    appTunerEx.Form[Self].AutoConstraints := True;

    appTunerEx.LoadProperties;
    appTunerEx.TuneComboboxes := True;
    acAllowDrag.Checked       := appTunerEx.Form[Self].AllowDrag;
    acBorderless.Checked      := appTunerEx.Form[Self].Borderless;
    seScale.Value             := appTunerEx.Form[Self].Scale;

    // load themes to theme selector
    cbTheme.Clear;
    cbTheme.Items.AddStrings(CAppTheme);
    cbTheme.ItemIndex := Integer(appTunerEx.Theme);
    cbTheme.Enabled   := appTunerEx.IsDarkThemeAvailable;
    cbTheme.OnChange  := @actionExecute;

    // load appTuner property values to controls
    acOnTop.Checked := appTunerEx.Form[Self].StayOnTop;

    // init controls
    InitSwitches;

    // go to connection page
    acConnect.Execute;

    AdjustTheme;

    // preserve window position when clicking on tray icon to hide/show
    Position := poDefault;
  end;

procedure TfmMain.FormMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
  begin
    if Shift = [ssCtrl] then
      begin
      if nbPages.PageIndex > tsGenerator.PageIndex then Exit;

      if WheelDelta > 0 then
        acZoomIn.Execute else
        acZoomOut.Execute;
      end;

    Handled := True;
  end;

procedure TfmMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
  begin
    Settings.SyncValues;
    if CloseAction = caFree then
      begin
      SaveSwitches;
      Settings.Save;
      appTunerEx.SaveProperties;
      end;
  end;

procedure TfmMain.FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  begin
    appTunerEx.Form[Self].ProcessMouseDown(X, Y);
  end;

procedure TfmMain.FormMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  begin
    if (Shift = [ssLeft]) and (WindowState = wsNormal) then
      appTunerEx.Form[Self].ProcessMouseMove(X, Y);
  end;

procedure TfmMain.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  begin
    appTunerEx.Form[Self].ProcessMouseUp(X, Y);
  end;



procedure TfmMain.actionCommandExecute(Sender: TObject);
  var
    a: TAction;
  begin
    BeginFormUpdate;
    a := TAction(Sender);

    case a.Name of

      'acModeRLC', 'acModeVoltmeter', 'acModeDiode', 'acModeGenerator':
        begin
        a.Checked     := True;
        tweezers.Mode := TTweezersMode(a.Tag);
        SelectPage(a.Tag);
        TableSelectRow;
        UpdateShortcuts;
        end;

      'acMFAuto', 'acMF95', 'acMF1k', 'acMF10k', 'acMF95k', 'acMF160k':
        begin
        a.Checked      := True;
        tweezers.MeasF := TTweezersMeasFreq(a.Tag);
        TableSelectRow;
        end;

      'acEqAuto', 'acEqPar', 'acEqSer':
        begin
        a.Checked   := True;
        tweezers.Eq := TTweezersEqCircuit(a.Tag);
        end;

      'acBandAuto', 'acBand1', 'acBand2', 'acBand3':
        begin
        a.Checked     := True;
        tweezers.Band := TTweezersBand(a.Tag);
        end;

      'acDiodeBiasOff', 'acDiodeBias6V', 'acDiodeBias26V':
        begin
        a.Checked       := True;
        tweezers.Offset := TTweezersOffset(a.Tag);
        end;

      'acGenSine30', 'acGenSine300', 'acGenSine3000', 'acGenPwm50',
      'acGenPwm2k', 'acGenPwm50k', 'acGenSerial', 'acGenNoise':
        begin
        a.Checked         := True;
        tweezers.Gen      := TTweezersGenMode(a.Tag);
        lbGenHint.Caption := a.Hint;
        imSpecial.GetBitmap(imgGen.Tag + a.Tag, imgGen.Picture.Bitmap);
        end;

      'acGenInc', 'acGenDec', 'acGenInc10', 'acGenDec10':
        tweezers.GenParameterChange(a.Tag);

      'acRelative':
        tweezers.Relative := TTweezersRelative(a.Checked);

      'acResetCounter':
        tweezers.ResetCounter;

      end;

    // mode specific init
    case a.Name of

      'acModeRLC':
        if not cbCalRLC.Checked then acBandAuto.Execute;

      'acModeVoltmeter':
        actionCalibrateExecute(cbCalU26Out);

      'acModeDiode':
        begin
        if acDiodeBiasOff.Checked then acDiodeBiasOff.Execute;
        if acDiodeBias6V.Checked then acDiodeBias6V.Execute;
        if acDiodeBias26V.Checked then acDiodeBias26V.Execute;
        end;
      end;

    EndFormUpdate;
  end;

procedure TfmMain.actionCalibrateExecute(Sender: TObject);
  var
    a: TComponent;
    c: TTweezersCalType;

  procedure WriteCalValue(AParam: TTweezersCalType; ACtrl: TControl; AMult: Double = 0);
    begin
      if Assigned(ACtrl) then
        begin
        ACtrl.Color := statusColors[tsOnline];

        if (ACtrl.ClassName = 'TFloatSpinEdit') then
          AMult *= TFloatSpinEdit(ACtrl).Value;
        end;

      tweezers.Calibrate(AParam, Round(AMult));
    end;
  begin
    BeginFormUpdate;
    a := TComponent(Sender);
    c := TTweezersCalType(a.Tag);

    // set calibration window caption
    fmCalibration.Caption := TXT_WINCAL_CAP[a.Name = 'acCalOSL'];

    case a.Name of

      'acCalibrate':
        begin
        SelectPage(tsCalibration.PageIndex);
        tweezers.Mode := tmRLC;
        end;

      'acCalRlow':
        WriteCalValue(c, fseCalRes12, 10);

      'acCalRhigh':
        WriteCalValue(c, fseCalRes3, 1000);

      'acCalUhigh':
        WriteCalValue(c, fseCalUhigh, 1);

      'acCalXtal':
        WriteCalValue(c, fseCalXtal, 10000);

      'acCalUlow':
        WriteCalValue(c, edCalUlow);

      'acCalOS', 'acCalOSL':
        tweezers.Calibrate(c, 0);

      'fseCalRes12', 'fseCalRes3', 'fseCalUhigh', 'fseCalXtal':
        TControl(a).Color := statusColors[tsOffline];

      'cbCalU26Out':
        tweezers.Offset := (cbCalUF.Checked and cbCalU26Out.Checked).Select(to26V, toDisable);

      end;

    EndFormUpdate;
  end;

procedure TfmMain.actionExecute(Sender: TObject);
  var
    a: TComponent;
    f: Text;
  begin
    BeginFormUpdate;
    a := TComponent(Sender);

    if a.ClassName = 'TAction' then
      if TAction(a).GroupIndex = 10 then TAction(a).Checked := True;

    case a.Name of

      'acStop':
        tweezers.Port := '';

      'acZoomIn', 'acZoomOut':
        if nbPages.PageIndex <= tsGenerator.PageIndex then
          begin
          if (a.Name = 'acZoomIn') and (fontSize < Scale96ToScreen(FONT_SIZE_MAX)) then  fontSize += 1;
          if (a.Name = 'acZoomOut') and (fontSize > Scale96ToScreen(FONT_SIZE_MIN)) then fontSize -= 1;
          AdjustSizes;
          end;

      'acQuit':
        Close;

      'acConnect':
        SelectPage(tsConnect.PageIndex);

      'acAbout':
        SelectPage(tsAbout.PageIndex);

      'acKeyMap':
        SelectPage(tsShortcuts.PageIndex);

      'acSettings':
        SelectPage(tsSettings.PageIndex);

      'acSettingsReset':
        if fmConfirm.Show(TXT_RST_CAPTION, TXT_RST_MESSAGE, mbYesNo, Self, mbNo) = mrYes then
          begin
          // simply clear content in settings file
          AssignFile(f, Settings.IniFile);
          Rewrite(f);
          CloseFile(f);
          Settings.IniFile   := '';
          appTunerEx.IniFile := '';
          Close;
          end;

      'acHold', 'cbHoldOnSpace':
        begin
        acHold.Checked := (tweezers.Status = tsOnline) and acHold.Checked and cbHoldOnSpace.Checked;
        lbHold.Tag     := 0;
        end;

      'acLogWrite':
        if cbLogEnable.Checked then
          begin
          actionExecute(cbLogEnable);

          if cbLogContinuous.Checked and (logTimeout < Now) then
            acLogWrite.Checked := False
          else
            begin
            ShowLogBanner(not cbLogContinuous.Checked, not acLogWrite.Checked);
            if not cbLogContinuous.Checked then UpdateLog;
            end;

          end
        else
          acLogWrite.Checked := False;

      'acLogOpen':
        OpenDocument(logger.Filename);

      'acHelp':
        OnLinkAboutClick(lbLinkHelp);

      'dlgFileLog':
        logger.Filename := dlgFileLog.FileName;

      'cbFonts':
        begin
        AdjustTheme;
        AdjustSizes;
        end;

      'cbLogEnable', 'cbLogContinuous', 'seLogTimeoutMM', 'seLogTimeoutSS':
        begin
        if not cbLogEnable.Checked then acLogWrite.Checked := False;
        logTimeout := Now + (seLogTimeoutSS.Value / 60 + seLogTimeoutMM.Value - 1e-3) / 60 / 24;
        end;

      'cbLanguage':
        appLocalizerEx.CurrentLanguage := cbLanguage.ItemIndex;

      'cbTheme':
        appTunerEx.Theme := TAppTheme(cbTheme.ItemIndex);

      'cbSticky':
        ScreenSnap := cbSticky.Checked;

      'acAllowDrag':
        appTunerEx.Form[Self].AllowDrag := acAllowDrag.Checked;

      'seScale':
        appTunerEx.Scale := seScale.Value;

      'imgLogo':
        with lbSysInfo do
          begin
          Tag         := 1;
          Transparent := False;
          Color       := $FFDDCC;
          Font.Color  := $00BACA;
          Font.Bold   := True;
          Caption     := // :)
            #239#187#191#208#160#208#158#208#151#208#160#208#158#208#145#208 +
            #155#208#149#208#157#208#158#32#208#146#32#208#163#208#154#208 +
            #160#208#144#208#135#208#157#208#134#13#208#156#208#184#209#128 +
            #209#131#32#209#130#208#176#32#209#137#208#176#209#129#209#130 +
            #209#143#32#209#131#209#129#209#150#208#188#32#208#177#208#181 +
            #208#183#32#208#178#208#184#208#189#209#143#209#130#208#186#209 +
            #131#13#208#151#208#176#208#178#208#182#208#180#208#184#32#208 +
            #191#208#190#209#129#209#130#209#131#208#191#208#176#208#185#32 +
            #208#183#32#208#187#209#142#208#180#209#140#208#188#208#184#32 +
            #209#130#208#176#208#186#44#32#209#143#208#186#32#209#133#208 +
            #190#209#130#209#150#208#178#32#208#177#208#184#44#32#209#137#208 +
            #190#208#177#32#208#191#208#190#209#129#209#130#209#131#208#191 +
            #208#176#208#187#208#184#32#208#183#32#209#130#208#190#208#177 +
            #208#190#209#142;
          end;

      end;

    UpdateShortcuts;
    EndFormUpdate;

    if a.Name = 'acOnTop' then
      appTunerEx.Form[Self].StayOnTop := acOnTop.Checked;

    if a.Name = 'acBorderless' then
      appTunerEx.Form[Self].Borderless := acBorderless.Checked;
  end;

procedure TfmMain.linkActionExecute(Sender: TObject);
  var
    a: TComponent;
  begin
    BeginFormUpdate;
    a := TComponent(Sender);

    case a.Name of

      'lbKeyMap':
        acKeyMap.Execute;

      'lbCalibration':
        acCalibrate.Execute;

      'lbResetSettings':
        acSettingsReset.Execute;

      'lbGenInc10':
        acGenInc10.Execute;

      'lbGenDec10':
        acGenDec10.Execute;

      'lbLogOpen':
        acLogOpen.Execute;

      end;

    EndFormUpdate;
  end;



procedure TfmMain.tmrTimerTimer(Sender: TObject);
  const
    prevStatus: TTweezersStatus = tsOffline;
    tmr1000msCnt: Integer       = 0;
  var
    a: TControl;
  begin
    BeginFormUpdate;

    case tweezers.Status of
      tsOffline:
        begin
        if tweezers.Port.IsEmpty then
          pbPort.Style := pbstNormal;
        end;

      tsOnline:
        begin
        if (cbModeGo.Checked) and (prevStatus <> tsOnline) then
          ShowModePage(tweezers.Mode);

        pbPort.Style := pbstNormal;
        end;

      tsScanning:
        begin
        pbPort.Style := pbstMarquee;
        end;
      end;

    prevStatus := tweezers.Status;

    // controls availability
    fmMain.Enabled      := not fmCalibration.Visible;
    acStop.Enabled      := not tweezers.Port.IsEmpty;
    pLogTimeout.Enabled := cbLogContinuous.Checked;
    ActionsEnable([acZoomIn, acZoomOut, acHold], nbPages.PageIndex <= tsGenerator.PageIndex);
    ActionsEnable([acCalOS, acCalOSL, acCalRhigh, acCalRlow, acCalUhigh, acCalUlow, acCalXtal], tweezers.Status = tsOnline);

    // update serial ports list only if ports page is visible
    SerialEnumerator.Update := nbPages.PageIndex = tsConnect.PageIndex;

    UpdateBanners;
    UpdateStatus;

    // 1 second sub-timer
    if tmr1000msCnt = 0 then
      begin
      tmr1000msCnt := (1000 div tmrTimer.Interval) - 1;

      // update system info ('About' page)
      if lbSysInfo.Tag = 0 then
        lbSysInfo.Caption := Format('System info' + LineEnding + 'OS %s' + LineEnding +
          'Screen: %d x %d, %d PPI. Window: %d x %d. Values font: %d',
          [appAbout.AppSystemStr, Screen.Width, Screen.Height, Screen.PixelsPerInch, Width, Height, fontSize]);

      // show tooltips
      for a in [
          bvRLC, bvFrequency, bvDiode, bvGenerator,
          lbHintR, lbHintV, lbHintD, lbHintG] do
        a.Visible := cbTooltips.Checked;

      // show debug tools if needed
      pCalRLC.Visible          := cbCalRLC.Checked;
      bvRLC.Visible            := not pCalRLC.Visible;
      lbHintR.Visible          := not pCalRLC.Visible;
      pCalUF.Visible           := cbCalUF.Checked;
      pFreqDN.Visible          := not pCalUF.Visible;
      bvFrequency.Visible      := not pCalUF.Visible;
      lbHintV.Visible          := not pCalUF.Visible;
      sgRLC.Columns[7].Visible := pCalRLC.Visible;
      end
    else
      Dec(tmr1000msCnt);

    EndFormUpdate;
  end;



procedure TfmMain.lsPortKeyPress(Sender: TObject; var Key: char);
  begin
    if key in [' ', #13] then
      lsPortSelectionChange(Sender, True);
  end;

procedure TfmMain.lsPortSelectionChange(Sender: TObject; User: Boolean);
  begin
    if User and (lsPort.ItemIndex in [0..lsPort.Count - 1]) then
      begin
      tweezers.Port := lsPort.Items[lsPort.ItemIndex];
      pbPort.Style  := pbstMarquee;
      end;
  end;

procedure TfmMain.lsPortDrawItem(Control: TWinControl; Index: Integer; ARect: TRect; State: TOwnerDrawState);
  var
    x, y, w: Integer;
    b:       String;
  begin
    with lsPort.Canvas do
      begin
      Pen.Color := Brush.Color;
      Rectangle(ARect);

      x := ARect.Left;
      y := ARect.CenterPoint.Y - TextHeight('1') div 2;
      b := ' ■ ';
      x += TextWidth(b);
      w := TextWidth('COM000 ');

      if Pen.Color <> clHighlight then
        begin
        Pen.Color := clGrayText;
        Line(x + w, ARect.Top, x + w, ARect.Bottom);
        end;

      w += 8;

      if Index = 0 then
        begin
        // draw 'auto-search' item
        TextRect(ARect, x + w, y, TXT_AUTOSCAN);
        Exit;
        end;

      // draw 'busy' mark
      Index -= 1;
      TextRect(ARect, ARect.Left, y, SerialEnumerator.Busy[Index].Select(b, ''));

      // draw port number
      if Index < SerialEnumerator.Count then
        TextRect(ARect, x, y, SerialEnumerator[Index]);

      // draw port friendly name
      TextRect(ARect, x + w, y, SerialEnumerator.Description[Index]);
      end;
  end;


procedure TfmMain.sgRLCAfterSelection(Sender: TObject; aCol, aRow: Integer);
  begin
    sgRLC.OnAfterSelection := nil;
    TableSelectRow;
    sgRLC.OnAfterSelection := @sgRLCAfterSelection;
  end;

procedure TfmMain.sgRLCDrawCell(Sender: TObject; aCol, aRow: Integer; aRect: TRect; aState: TGridDrawState);
  var
    _item:  TTweezersItem;
    bmpRLC: TBitmap;
  begin
    if aRow < sgRLC.FixedRows then Exit;

    // draw cap/ind image in 'behavior' column
    if aCol = 4 then
      begin
      _item := TTweezersItem(StrToInt(sgRLC.Cells[aCol, aRow]));
      if _item in [tiCapacitor, tiInductor] then
        begin
        bmpRLC := imSVGList.List.GetBGRABitmap(
          (_item = tiCapacitor).Select(IMG_CAP_ID, IMG_IND_ID),
          sgRLC.RowHeights[aRow] * 120 div 100).Bitmap;
        sgRLC.Canvas.Draw(
          aRect.CenterPoint.X - bmpRLC.Width div 2,
          aRect.CenterPoint.Y - bmpRLC.Height div 2, bmpRLC);
        bmpRLC.Free;
        end;
      end;
  end;

procedure TfmMain.sgRLCKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  begin
    // save current row to prevent moving by left/right arrow keys
    prevRow := (Key in [VK_LEFT, VK_RIGHT]).Select(sgRLC.Row, -1);
    AppOnKeyDown(Sender, Key, Shift);
  end;

procedure TfmMain.sgRLCPrepareCanvas(Sender: TObject; aCol, aRow: Integer; aState: TGridDrawState);
  begin
    sgRLC.Canvas.Font.Color := sgRLC.Tag;

    // hide text in "behavior" column, for smoothly images drawing
    if aCol = 4 then
      sgRLC.Canvas.Font.Color := sgRLC.Canvas.Brush.Color;
  end;


procedure TfmMain.stbStatusResize(Sender: TObject);
  var
    _w: Integer = 0;
    i:  Integer;
  begin
    for i := 1 to stbStatus.Panels.Count - 1 do
      _w += stbStatus.Panels[i].Width;

    stbStatus.Panels[0].Width := stbStatus.Width - _w;
  end;

procedure TfmMain.tiTrayClick(Sender: TObject);
  begin
    if WindowState <> wsMinimized then
      Visible := not Visible;
  end;



procedure TfmMain.OnTweezersConnect(Sender: TObject);
  begin
    acHold.Checked     := False;
    acLogWrite.Checked := False;
    logHeader          := True;
  end;

procedure TfmMain.OnTweezersDisconnect(Sender: TObject);
  begin
    OnTweezersInfo(Sender);
  end;

procedure TfmMain.OnTweezersUpdate(Sender: TObject);
  var
    _row:   TTweezersMeasFreq;
    _imgId: Integer = 0;
    _item:  TTweezersItem;
    _serc:  Boolean;

  procedure SetRLCTableRow(F: TTweezersMeasFreq; ColStr: TStringArray; IsFmt: TBooleanDynArray);
    var
      i: Integer;
    begin
      if Length(ColStr) <> Length(IsFmt) then Exit;
      for i := 0 to sgRLC.ColCount - 1 do
        if i < Length(ColStr) then
          sgRLC.Cells[i, Ord(F)] := IsFmt[i].Select(FormatNumber(ColStr[i]), ColStr[i]) else
          sgRLC.Cells[i, Ord(F)] := '';
    end;

  procedure SetValueLabels(ALabels: array of TLabel; ACols: array of Integer);
    var
      i: Integer;
    begin
      for i := 0 to High(ALabels) do
        if i < Length(ACols) then
          ALabels[i].Caption := sgRLC.Cells[ACols[i], sgRLC.Row] else
          ALabels[i].Caption := '';
    end;

  procedure SetValueLabels(ALabels: array of TLabel; AValues: TStringArray);
    var
      i: Integer;
    begin
      for i := 0 to High(ALabels) do
        if i < Length(AValues) then
          ALabels[i].Caption := FormatNumber(AValues[i]) else
          ALabels[i].Caption := '';
    end;
  begin
    BeginFormUpdate;

    case tweezers.Mode of

      // update data in RLC mode
      tmRLC:
        begin

        // update RLC table from tweezers actual data
        if not acHold.Checked then
          for _row := tf95 to tf160k do
            with tweezers.RLC[_row] do
              SetRLCTableRow(_row, [MF, '   ' + TXT_EQ_CIRC[EqCct] + '   ', R, CL, Ord(Item).ToString, QT, Z, BUI],
                [True, False, True, True, False, True, True, False]);

        // update big labels from table data
        SetValueLabels(
          [lbValueMF, lbValueEq, lbValueR, lbValueLC, lbValueQ, lbValueZ, lbValueBUI],
          [0, 1, 2, 3, 5, 6, 7]);

        _item := TTweezersItem(StrToInt(sgRLC.Cells[4, sgRLC.Row]));
        _serc := sgRLC.Cells[1, sgRLC.Row].Contains(TXT_EQ_CIRC[teSerial]);

        lbLabelLC.Caption := TXT_LABEL_LC[_item = tiCapacitor];
        lbLabelQ.Caption  := TXT_LABEL_Q[_item = tiCapacitor];

        // update big image for eq. circuit
        if _item in [tiCapacitor, tiInductor] then
          begin
          if _item = tiInductor then _imgId += 2;
          if _serc then _imgId              += 1;
          imSpecial.GetBitmap(imgEq.Tag + _imgId, imgEq.Picture.Bitmap);
          end
        else
          imgEq.Picture.Clear;
        end;

      // update data in UFD mode
      tmVoltmeter, tmDiode:
        if not acHold.Checked then
          begin
          with tweezers.F do
            SetValueLabels(
              [lbValueF, lbValueT, lbValueD, lbValueN],
              [F, T, D, N]);

          with tweezers.V do
            SetValueLabels(
              [lbValueVrms, lbValueVavg, lbValueVpp, lbValueVd],
              [RMS, avg, pp, avg]);
          end;

      // update data in GEN mode
      tmGenerator:
        SetValueLabels(
          [lbValueGP, lbValueGV],
          [TXT_GEN_LABEL[tweezers.Gen], tweezers.G.Value]);
      end;

    UpdateLog;
    EndFormUpdate;
  end;

procedure TfmMain.OnTweezersInfo(Sender: TObject);
  procedure SetCompColor(ACtrlArr: array of TControl; AColor: TColor);
    var
      a: TControl;
    begin
      for a in ACtrlArr do a.Color := AColor;
    end;
  begin
    // update fields with cal. data
    if cbCalRead.Checked and (tweezers.Status = tsOnline) then
      with tweezers.Info do
        begin
        fseCalRes12.Value := StrToIntDef(Rcal12, 1500) / 10;
        fseCalRes3.Value  := StrToIntDef(Rcal3, 20000) / 1000;
        fseCalXtal.Value  := StrToIntDef(Fz, 80000000) / 10000;
        end;

    // this fields were read so they are green (like 'online' status)
    SetCompColor([fseCalRes12, fseCalRes3, fseCalXtal],
      cbCalRead.Checked.Select(statusColors[tsOnline], clDefault));

    // this fields were not read so they are default color
    SetCompColor([fseCalUhigh, edCalUlow], clDefault);
  end;

procedure TfmMain.OnTweezersCalibrate(Sender: TObject);
  begin
    fmCalibration.SetMessage(TXT_CAL_LABEL[tweezers.Info.CalStage]);

    case tweezers.Info.CalStage of

      tcsStart:
        begin
        fmCalibration.Show;
        if fmCalibration.ContinuedByUser(True) then
          tweezers.CalibrationContinue;
        end;

      tcsRes12, tcsRes3:
        begin
        fmCalibration.ContinuedByUser(True);
        tweezers.CalibrationContinue;
        end;

      tcsDone:
        begin
        fmCalibration.AllowClose;
        if cbCalAutoClose.Checked then fmCalibration.Close;
        acMFAuto.Execute;
        end;

      tcsError:
        fmCalibration.AllowClose;

      tcsWait:
        fmCalibration.ContinuedByUser;

      end;
  end;



function TfmMain.FormatNumber(S: String): String;
  begin
    Result := ModifyNumber(S,
      TTweezersDecimal(cbDecimal.ItemIndex),
      TTweezersThousand(cbThousand.ItemIndex));

    Result := UnitsReplace(Result,
      cbUnitsSep.Checked,
      TTweezersUnits(cbUnits.ItemIndex));
  end;

procedure TfmMain.ActionsEnable(AActions: array of TAction; AEnable: Boolean);
  var
    _item: TAction;
  begin
    for _item in AActions do
      _item.Enabled := AEnable;
  end;

procedure TfmMain.AppOnKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

  procedure DoAction(AIndex: Integer; AActions: array of TAction);
    begin
      if AIndex > High(AActions) then AIndex := High(AActions);
      if AIndex < Low(AActions) then AIndex  := Low(AActions);
      if Assigned(AActions[AIndex]) then AActions[AIndex].Execute;
    end;
  var
    dx: Integer = 0;
    dy: Integer = 0;
  begin
    if Key = VK_RIGHT then dx := 1;
    if Key = VK_LEFT then dx  := -1;
    if Key = VK_UP then dy    := 1;
    if Key = VK_DOWN then dy  := -1;

    if (dx = 0) and (dy = 0) then Exit;

    case pcPages.Pages[nbPages.PageIndex].Name of

      'tsRLCmeter':
        DoAction(Integer(tweezers.MeasF) + dx,
          [acMFAuto, acMF95, acMF1k, acMF10k, acMF95k, acMF160k]);

      'tsDiodeTest':
        DoAction(Integer(tweezers.Offset) + dx,
          [acDiodeBiasOff, acDiodeBias6V, acDiodeBias26V]);

      'tsGenerator':
        begin
        DoAction(Integer(tweezers.Gen) + dx,
          [acGenSine30, acGenSine300, acGenSine3000, acGenPwm50, acGenPwm2k, acGenPwm50k, acGenSerial, acGenNoise]);

        DoAction(1 + dy,
          [acGenDec, nil, acGenInc]);
        end;
      end;
  end;

procedure TfmMain.OnValueClick(Sender: TObject);
  begin
    if TComponent(Sender).ClassName <> 'TLabel' then Exit;
    Clipboard.AsText := TLabel(Sender).Caption;

    if not cbHintShow.Checked then Exit;
    tiTray.BalloonTitle := Application.Title;
    tiTray.BalloonHint  := Format(TXT_COPIED, [Clipboard.AsText]);
    tiTray.ShowBalloonHint;
  end;

procedure TfmMain.OnLinkAboutClick(Sender: TObject);
  begin
    appAbout.linkClick(Sender);
  end;

procedure TfmMain.ShowModePage(AMode: TTweezersMode);
  begin
    case AMode of
      tmRLC: acModeRLC.Execute;
      tmVoltmeter: acModeVoltmeter.Execute;
      tmDiode: acModeDiode.Execute;
      tmGenerator: acModeGenerator.Execute;
      end;

    TableSelectRow;
  end;

procedure TfmMain.ShowLogBanner(ASingle, AEnd: Boolean);
  begin
    if AEnd then
      lbLogged.Caption := TXT_LOG_END
    else
      lbLogged.Caption := ASingle.Select(TXT_LOG_ADD, TXT_LOG_BEGIN);

    lbLogged.Tag := TMR_LOGGED;
  end;



procedure TfmMain.TableSelectRow;
  begin
    if not Enabled then Exit;

    sgRLC.Visible := cbTableShow.Checked or (tweezers.MeasF = tfAuto);

    if sgRLC.Visible and nbPages.Page[tsRLCmeter.PageIndex].Visible then
      sgRLC.SetFocus;

    // prevent row changing by left/right arrow keys
    if prevRow <> -1 then
      sgRLC.Row := prevRow;

    // change row only when non-AUTO frequency selected
    if tweezers.MeasF <> tfAuto then
      sgRLC.Row := Ord(tweezers.MeasF);

    OnTweezersUpdate(Self);
  end;

procedure TfmMain.SelectPage(AIndex: Integer);
  begin
    nbPages.PageIndex := AIndex;

    if AIndex in [tsRLCmeter.PageIndex .. tsDiodeTest.PageIndex] then
      begin
      lbHold.Parent   := nbPages.ActivePageComponent;
      lbLogged.Parent := nbPages.ActivePageComponent;
      end;

    if AIndex in [tsConnect.PageIndex, tsAbout.PageIndex] then
      pLinks.Parent := nbPages.ActivePageComponent;

    if AIndex in [tsConnect.PageIndex] then
      if lsPort.Visible then lsPort.SetFocus;

    lbSysInfo.BringToFront;
  end;

procedure TfmMain.UpdatePortsList(Sender: TObject);

  procedure UpdateStrings;
    var
      i: Integer;
    begin
      with lsPort do
        begin
        Tag := ItemIndex;
        Clear;
        Items.Add(TXT_AUTOSCAN);
        for i := 0 to SerialEnumerator.Count - 1 do
          Items.Add(SerialEnumerator[i]);
        ItemIndex := Tag;
        Visible   := True;
        end;
    end;

  function SelectedPortIndex: Integer;
    var
      i: Integer;
    begin
      Result := 0;
      with lsPort do
        if Count > 1 then
          for i := 1 to Count - 1 do
            if Items[i] = tweezers.Port then
              Exit(i);
    end;
  begin
    BeginFormUpdate;

      try
      if SerialEnumerator.Count = 0 then lsPort.Visible := False
      else
      if SerialEnumerator.Updated then UpdateStrings;

      with lsPort do
        if (tweezers.Status = tsOnline) and (Tag = ItemIndex) then
          ItemIndex := SelectedPortIndex;
      except
      end;

    EndFormUpdate;
  end;

procedure TfmMain.UpdateStatus;
  const space = '  ';

  procedure Field(AIndex: Integer; AData: String; AShow: Boolean = True);
    begin
      if AIndex < stbStatus.Panels.Count then
        stbStatus.Panels[AIndex].Text := AShow.Select(space + AData + space, '');
    end;
  begin
    Field(1, Format(TXT_LOG_DOING, [TimeToStr(logTimeout - Now, appFmtSet)]), acLogWrite.Checked);
    Field(2, 'FW v' + tweezers.Info.FWmajor, tweezers.Info.FWmajor <> '');
    Field(3, tweezers.Port, tweezers.Status = tsOnline);
    Field(4, TXT_STATUS[tweezers.Status]);
    Field(5, acHold.Checked.Select(TXT_VAL_HOLD, TXT_VAL_ACTIVE), (tweezers.Status = tsOnline) and not lbHold.Visible);

    lbConnect.Caption := TXT_STATUS[tweezers.Status];
    lbConnect.Color   := statusColors[tweezers.Status];
  end;

procedure TfmMain.UpdateLog;
  const
    logw: array of Integer = (10, 13, 13, 13, 13, 24, 15, 10); // log fields widths
    tCMax: Integer         = 32; // max lines in block
    tCntr: Integer         = 0;
  var
    _row: TTweezersMeasFreq;

  function GetEntry(ACols: TStringArray; IsFormat: Boolean = True): String;
    var
      i: Integer;
      s: String;
    begin
      Result := '';
      for i  := 0 to High(ACols) do
        if i < Length(logw) then
          begin
          s      := IsFormat.Select(FormatNumber(ACols[i]), ACols[i]);
          Result += UnicodeFormat(' %' + IntToStr(logw[i]) + 's |', [s]);
          end;
    end;

  procedure AddTitle(ACols: TStringArray);
    const
      prevM: Integer = -1;
    begin
      if (tCntr = 0) or (Integer(tweezers.Mode) <> prevM) then
        begin
        logger.Log('');
        logger.Log('');
        logger.Log(GetEntry(ACols, False));
        tCntr := tCMax;
        end;

      if cbLogEnable.Checked then
        prevM := Integer(tweezers.Mode);
    end;

  procedure Log(ACols: TStringArray);
    const
      prevE: String = '';
    var
      entry: String;
    begin
      ACols[0] := STR_TWZ_MODE[tweezers.Mode];
      entry    := GetEntry(ACols);

      if entry <> prevE then
        begin
        logger.Log(entry);
        Dec(tCntr);
        end;

      prevE := entry;
    end;
  begin
    logger.Active := cbLogEnable.Checked;

    if not cbLogEnable.Checked then Exit;
    if not acLogWrite.Checked then Exit;

    // add header at log starting
    if logHeader then
      begin
      logHeader := False;
      logger.Log('');
      logger.Log('');
      logger.Log('Log session started. Log encoding: UTF-8. Device: <%s> FW v%s @ %s',
        [tweezers.MODEL_ID, tweezers.Info.FWmajor, tweezers.Port]);
      logger.Log('Application: %s',
        [appAbout.UpdateAboutAppShort.Trim([#10, #13, ' ']).Replace(LineEnding, ', ')]);
      end;

    // write log in different modes
    case tweezers.Mode of

      tmRLC:
        begin
        AddTitle(['Mode', 'R', 'C/L', 'Q/tg', 'Z', 'Eq.Circ.', 'Meas.F']);

        for _row := tfAuto to tf160k do
          if ({cbLogContinuous.Checked and} (tweezers.MeasF = tfAuto)) xor (_row = tfAuto) then
            with tweezers.RLC[_row] do
              Log(['', R, CL, QT, Z, E, (tweezers.MeasF = tfAuto).Select('* ', '') + MF]);
        end;

      tmVoltmeter:
        begin
        AddTitle(['Mode', 'Vrms', 'Vavg', 'Vpp', 'Duty c.', 'Frequency', 'Period', 'Pulses']);
        with tweezers.F, tweezers.V do
          Log(['', RMS, avg, pp, D, F, T, N]);
        end;

      tmDiode:
        begin
        AddTitle(['Mode', 'Vavg']);
        with tweezers.V do
          Log(['', avg]);
        end;

      tmGenerator:
        begin
        AddTitle(['Mode', '-', '-', '-', 'Gen.mode', 'Parameter', 'Value']);
        with tweezers.G do
          Log(['', '', '', '', STR_TWZ_GENM[tweezers.Gen], TXT_GEN_LABEL[tweezers.Gen], Value]);
        end;
      end;

    // disable log when needed
    if not cbLogContinuous.Checked or (logTimeout < Now) then
      begin
      acLogWrite.Checked := False;
      if cbLogContinuous.Checked then ShowLogBanner(False, True);
      end;
  end;

procedure TfmMain.UpdateBanners;
  begin
    // log banner visibility control
    with lbLogged do
      begin
      Visible             := cbLogEnable.Checked and (Tag > 0);
      if Tag > 0 then Tag := Tag - 1;
      end;

    // "HOLDED" banner visibility control
    with lbHold do
      if Tag > 0 then
        Tag     := Tag - 1
      else
        begin
        Visible := not Visible and acHold.Checked;
        Tag     := TMR_HOLDED div tmrTimer.Interval;
        end;
  end;

procedure TfmMain.UpdateShortcuts;

  procedure SetShortcuts(AActions: array of TAction; AShortcuts: array of TShortCut; AApply: Boolean);
    var
      i: Integer;
    begin
      if Length(AActions) = 0 then Exit;
      if Length(AShortcuts) = 0 then Exit;
      if Length(AActions) <> Length(AShortcuts) then Exit;
      for i := 0 to High(AActions) do
        AActions[i].ShortCut := AApply.Select(AShortcuts[i], 0);
    end;
  var
    _page: TTabSheet;
  begin
    _page := pcPages.Pages[nbPages.PageIndex];

    // shortcuts on 'RLC mode' page
    SetShortcuts(
      [acMFAuto, acMF95, acMF1k, acMF10k, acMF95k, acMF160k, acEqAuto, acEqPar, acEqSer, acRelative],
      [VK_A, VK_S, VK_D, VK_F, VK_G, VK_H, VK_J, VK_K, VK_L, VK_R],
      _page = tsRLCmeter);

    // shortcuts on 'Voltmeter mode' page
    SetShortcuts(
      [acResetCounter],
      [VK_R],
      _page = tsVoltmeter);

    // shortcuts on 'DiodeTest mode' page
    SetShortcuts(
      [acDiodeBiasOff, acDiodeBias6V, acDiodeBias26V],
      [VK_A, VK_S, VK_D],
      _page = tsDiodeTest);

    // shortcuts on 'Generator mode' page
    SetShortcuts(
      [acGenSine30, acGenSine300, acGenSine3000, acGenPwm50, acGenPwm2k, acGenPwm50k, acGenSerial, acGenNoise],
      [VK_A, VK_S, VK_D, VK_F, VK_G, VK_H, VK_J, VK_K],
      _page = tsGenerator);

    // shortcuts on all mode pages
    SetShortcuts(
      [acZoomIn, acZoomOut, acLogWrite],
      [VK_LCL_EQUAL, VK_LCL_MINUS, VK_RETURN],
      nbPages.PageIndex <= tsGenerator.PageIndex);

    // shortcuts on all mode pages except 'Generator mode' page
    SetShortcuts(
      [acHold],
      [VK_SPACE],
      nbPages.PageIndex <= tsDiodeTest.PageIndex);

    // shortcuts on 'Connection' page
    SetShortcuts(
      [acStop],
      [VK_ESCAPE],
      nbPages.PageIndex = tsConnect.PageIndex);
  end;

procedure TfmMain.UpdateAbout;

  procedure AssignAppIconTo(APicture: TPicture);
    var
      _icon: TIcon;
    begin
      _icon := TIcon.Create;
      with _icon do
        begin
        Assign(Application.Icon);
        Current          := GetBestIndexForSize(Size(256, 256));
        APicture.Graphic := _icon;
        Free;
        end;
    end;

  procedure CreateLinks(ALabel: array of TLabel; AURL: array of String);
    var
      i: Integer;
    begin
      if Length(ALabel) <> Length(AURL) then Exit;
      if Length(ALabel) = 0 then Exit;
      for i := 0 to High(ALabel) do
        begin
        ALabel[i].Hint    := AURL[i];
        ALabel[i].OnClick := @OnLinkAboutClick;
        end;
    end;
  begin
    LoadPictureFromResource(imgLogo.Picture, 'LOGO');
    AssignAppIconTo(imgIcon.Picture);

    mmAbout.Lines.Text := appAbout.UpdateAboutAppInfo;
    Caption            := appAbout.AppProductStr;
    Application.Title  := Caption;
    lbSysInfo.OnClick  := @OnValueClick;

    CreateLinks(
      [lbLinkRep, lbLinkDev, lbLinkPage, lbLinkHelp, lbLinkLic, lbLinkReadme, lbLinkAndr, lbLinkDriver],
      [APP_REPO_ADDRESS, APP_OTHER_ADDRESS, APP_HOME_ADDRESS, FILE_HELP, FILE_LICENSE, FILE_README, APP_ANDROID_ADDRESS, APP_DRIVER_ADDRESS]);
  end;



procedure TfmMain.InitConfigComponents;
  begin
    Settings.Add(cbLanguage, @cfg.app.lang);
    Settings.Add(cbSticky, @cfg.app.sticky);
    Settings.Add(cbTooltips, @cfg.app.tooltips);
    Settings.Add(cbFonts, @cfg.val.font);
    Settings.Add(cbUnits, @cfg.val.units);
    Settings.Add(cbDecimal, @cfg.val.sepdec);
    Settings.Add(cbThousand, @cfg.val.sepths);
    Settings.Add(cbUnitsSep, @cfg.val.sepunits);
    Settings.Add(cbHoldOnSpace, @cfg.misc.hold);
    Settings.Add(cbHintShow, @cfg.misc.hint);
    Settings.Add(cbTableShow, @cfg.misc.table);
    Settings.Add(cbLogEnable, @cfg.log.enable);
    Settings.Add(cbLogContinuous, @cfg.log.cont);
    Settings.Add(seLogTimeoutMM, @cfg.log.mm);
    Settings.Add(seLogTimeoutSS, @cfg.log.ss);
    Settings.Add(dlgFileLog, @cfg.log.filename);
    Settings.Add(cbModeGo, @cfg.connect.modego);

    // do not save calibration options by design
    //Settings.Add(cbCalRead, @cfg.cal.readval);
    //Settings.Add(cbCalRLC, @cfg.cal.rlc);
    //Settings.Add(cbCalUF, @cfg.cal.uf);
    //Settings.Add(cbCalAutoClose, @cfg.cal.autoclose);

    // add positions of switches as settings to save/restore
    Settings.Add('SwitchMF', stInt, @cfg.work.fmeas, '0');
    Settings.Add('SwitchEQ', stInt, @cfg.work.eq, '0');
    Settings.Add('SwitchOffset', stInt, @cfg.work.offset, '0');
    Settings.Add('SwitchGen', stInt, @cfg.work.gen, '0');
  end;

procedure TfmMain.InitPages;
  var
    i, k: Integer;
  begin
    { In design time we use 'TPageControl:pcPages' for convenient GUI building.
      But in run-time there are some issues, such as unexpected flickering.
      So in run time we use 'TNotebook:nbPages' to show our GUI.
      Here we move all content from 'pcPages' to 'nbPages' pages. }

    pcPages.Align := alNone;
    pcPages.Hide;
    nbPages.Align := alClient;

    for i := 0 to pcPages.PageCount - 1 do
      with pcPages.Pages[i] do
        begin
        nbPages.Pages.Add('npPage' + Name);
        nbPages.Page[i].ChildSizing.Assign(ChildSizing);

        for k := 0 to ControlCount - 1 do
          Controls[0].Parent := nbPages.Page[i];
        end;
  end;

procedure TfmMain.InitSwitches;
  begin
    tbSwitchMF.Buttons[cfg.work.fmeas].Click;
    tbSwitchEq.Buttons[cfg.work.eq].Click;
    tbSwitchOffset.Buttons[cfg.work.offset].Click;
    tbSwitchGen.Buttons[cfg.work.gen].Click;

    //acMFAuto.Execute;
    //acEqAuto.Execute;
    //acDiodeBiasOff.Execute;
    //acGenSine30.Execute;        
    acBandAuto.Execute;
  end;

procedure TfmMain.SaveSwitches;
  begin
    cfg.work.fmeas  := Integer(tweezers.MeasF);
    cfg.work.eq     := Integer(tweezers.Eq);
    cfg.work.offset := Integer(tweezers.Offset);
    cfg.work.gen    := Integer(tweezers.Gen);
  end;

procedure TfmMain.InitLocalizer;
  begin
    appLocalizerEx.Localize(cbUnits, ITEMS_UNITSPREF);
    appLocalizerEx.Localize(cbDecimal, ITEMS_DECIMAL);
    appLocalizerEx.Localize(cbThousand, ITEMS_THOUSAND);
    appLocalizerEx.Localize(cbTheme, CAppTheme);
  end;



procedure TfmMain.OnLangChange(Sender: TObject);
  begin
    lbLogOpen.Caption := acLogOpen.Caption;

    InitLocalizer;
    AdjustTheme;
    AdjustSizes;
    UpdateAbout;
  end;



procedure TfmMain.AdjustTheme;
  var
    _img: TImage;
    i:    Integer;

  procedure SetLabelTheme(ALabels: array of TLabel; AColor: TColor = cl3DLight; AColorOnly: Boolean = False);
    var
      item: TLabel;
    begin
      for item in ALabels do
        begin
        item.Transparent := False;
        item.Color       := AColor;

        if not AColorOnly then
          begin
          item.Caption    := '---';
          item.Font.Name  := cbFonts.Text;
          item.Font.Pitch := fpFixed;
          item.Cursor     := crHandPoint;
          item.OnClick    := @OnValueClick;
          end;
        end;
    end;

  procedure SetToolbarTheme(AToolbars: array of TToolBar; ATransparent: Boolean = False);
    var
      item: TToolBar;
      i:    Integer;
    begin
      for item in AToolbars do
        begin
        item.Indent      := 0;
        item.Transparent := ATransparent;
        item.Color       := clForm;

        if not ATransparent then
          item.EdgeBorders := [ebBottom];

        for i := 0 to item.ButtonCount - 1 do
          item.Buttons[i].Cursor := crHandPoint;
        end;
    end;

  procedure SetSpeeButtonTheme(ASpeedButtons: array of TSpeedButton);
    var
      item: TSpeedButton;
    begin
      for item in ASpeedButtons do
        begin
        item.Cursor := crHandPoint;
        end;
    end;

  procedure SetEventsForDraggingByMouse;
    var
      i: Integer;
    begin
      for i := 0 to ComponentCount - 1 do
        case Components[i].ClassName of
          {$Macro on}
          {$Define SET_MOUSE_HANDLERS :=
              begin
                OnMouseDown := Self.OnMouseDown;
                OnMouseMove := Self.OnMouseMove;
                OnMouseUp   := Self.OnMouseUp;
              end }

          'TLabel': with TLabel(Components[i]) do SET_MOUSE_HANDLERS;
          'TPanel': with TPanel(Components[i]) do SET_MOUSE_HANDLERS;
          'TPage': with TPage(Components[i]) do SET_MOUSE_HANDLERS;
          'TImage': with TImage(Components[i]) do SET_MOUSE_HANDLERS;
          'TGroupBox': with TGroupBox(Components[i]) do SET_MOUSE_HANDLERS;
          'TStatusBar': with TStatusBar(Components[i]) do SET_MOUSE_HANDLERS;
          {$Macro off}
          end;
    end;
  begin
    BeginFormUpdate;

    imSVGList.Rendering     := False;
    imSVGListSpec.Rendering := False;

    if appTunerEx.IsDarkTheme then
      begin
      statusColors          := VAL_STATUS_COLOR_DARK;
      sgRLC.Color           := clMenuHighlight;
      sgRLC.AlternateColor  := clInactiveBorder;
      sgRLC.FixedColor      := clBtnShadow;
      sgRLC.SelectedColor   := cl3DDkShadow;
      Application.HintColor := $A9E7FF;
      pShortcuts.Color      := $555555;

      imSVGList.List.Text := imSVGList.List.Text
        .Replace('#000', '#ffe7a9')
        .Replace('stroke-width="1.7"', 'stroke-width="1.0"');
      end
    else
      begin
      nbPages.Color         := $FBFBFB;
      statusColors          := VAL_STATUS_COLOR;
      sgRLC.SelectedColor   := clScrollBar;
      Application.HintColor := $EFFFFF;
      end;

    imSVGList.Rendering     := True;
    imSVGListSpec.Rendering := True;

    SetLabelTheme([
      lbValueR, lbValueLC, lbValueZ, lbValueQ, lbValueMF, lbValueEq,
      lbValueVavg, lbValueVd, lbValueVpp, lbValueVrms,
      lbValueF, lbValueD, lbValueN, lbValueT, lbValueGV], nbPages.Color);
    SetLabelTheme([lbValueBUI], clForm);

    SetToolbarTheme([tbSwitchMF, tbSwitchEq, tbSwitchRel, tbSwitchOffset, tbSwitchGen]);
    SetToolbarTheme([tbMode, tbSystem], True);

    //SetLabelTheme([
    //  lbLabelR, lbLabelLC, lbLabelZ, lbLabelQ, lbLabelMF, lbLabelEq,
    //  lbLabelVavg, lbLabelVd, lbLabelVpp, lbLabelVrms,
    //  lbLabelRS, lbLabelF, lbLabelT, lbLabelD, lbLabelN, lbValueGP], clForm, True);

    SetSpeeButtonTheme([sbRstCnt, sbGenDec, sbGenInc]);

    tiTray.Icon := Application.Icon;
    tiTray.Hint := Application.Title;

    lbHold.Caption     := TXT_VAL_HOLD;
    lbGenInc10.Hint    := acGenInc10.Hint;
    lbGenDec10.Hint    := acGenDec10.Hint;
    lsPort.ParentColor := True;

    sgRLC.Tag        := sgRLC.Font.Color; // selection font color
    sgRLC.Font.Name  := cbFonts.Text;
    sgRLC.Font.Pitch := fpFixed;

    imSpecial.GetBitmap(imgDiode.Tag, imgDiode.Picture.Bitmap);

    for _img in [imgX1, imgX2] do
      begin
      _img.Picture.Bitmap.FreeImage;
      _img.Picture.Bitmap := imSVGListSpec.List.BGRASVGImageList.GetBGRABitmap(
        _img.Tag, lsPort.Height, lsPort.Height).RotateCW.Bitmap;
      end;

    svgKeymap.LoadFromResource('KEYMAP');

    // translate labels in SVG keymap
    for i := 0 to High(KEYMAP_LABELS) do
      svgKeymap.SVGString := svgKeymap.SVGString
        .Replace(KEYMAP_LABELS_ORIG[i], KEYMAP_LABELS[i]);

    SetEventsForDraggingByMouse;
    EndFormUpdate;
  end;

procedure TfmMain.AdjustSizes;

  procedure SetLabelConstrains(APatterns: array of String; ALabels: array of TLabel; AScaleFont: Boolean = True; W: Integer = 0);
    var
      item: TLabel;
      _s:   String;
    begin
      if Length(APatterns) = 0 then Exit;
      if Length(ALabels) = 0 then Exit;

      for item in ALabels do
        begin
        if AScaleFont then item.Font.Height := fontSize;

        if W = 0 then
          for _s in APatterns do
            W := Max(W, item.Canvas.GetTextWidth(_s));

        item.Constraints.MinWidth  := W;
        item.Constraints.MaxWidth  := item.Constraints.MinWidth;
        item.Constraints.MinHeight := item.Canvas.GetTextHeight(APatterns[0]);
        end;
    end;

  procedure SetPanelChildConstrains(APanel: TPanel);
    var
      i: Integer;
      w: Integer = 0;
    begin
      if APanel.ControlCount = 0 then Exit;

      for i := 0 to APanel.ControlCount - 1 do
        w := Max(w, APanel.Controls[i].Width);

      for i := 0 to APanel.ControlCount - 1 do
        with APanel.Controls[i].Constraints do
          begin
          MinWidth := w;
          MaxWidth := w;
          end;
    end;

  procedure SetToolbarConstrains(AToolbars: array of TToolBar; MinW: Integer = 1; MinH: Integer = -1);
    var
      item: TToolBar;
      i, w: Integer;
    begin
      for item in AToolbars do
        begin
        item.ButtonWidth := MinW;
        if MinH > 0 then item.ButtonHeight := MinH;
        end;

      for item in AToolbars do
        begin
        w := 0;

        for i := 0 to item.ButtonCount - 1 do
          w := Max(w, item.Buttons[i].Width);

        item.ButtonWidth := w + Scale96ToScreen(2);
        end;
    end;

  procedure SetSpeeButtonConstrains(ASpeedButtons: array of TSpeedButton; W: Integer = -1);
    var
      item: TSpeedButton;
    begin
      for item in ASpeedButtons do
        begin
        item.Constraints.MinWidth := 0;
        item.Constraints.MaxWidth := 0;
        end;

      for item in ASpeedButtons do
        with item.Constraints do
          begin
          if Assigned(item.Images) then
            MinHeight := item.Images.Width + 12 else
            MinHeight := item.Canvas.GetTextHeight('0') + 12;

          if W = -1 then
            begin
            MinWidth := Max(Max(item.Width, item.Height), MinHeight);
            MaxWidth := MinWidth;
            end
          else
            MinWidth := W;
          end;
    end;

  procedure SetStatusBarWidth(APanel: Integer; APatterns: array of String);
    var
      s: String;
    begin
      if APanel >= stbStatus.Panels.Count then Exit;
      if Length(APatterns) = 0 then Exit;
      for s in APatterns do
        stbStatus.Panels[APanel].Width :=
          Max(stbStatus.Panels[APanel].Width, stbStatus.Canvas.GetTextWidth(s + '----'));
    end;
  var
    _s:     String;
    _h, _w: Integer;
  begin
    Constraints.MinHeight    := Scale96ToScreen(450);
    Constraints.MinWidth     := Scale96ToScreen(620);
    imSVGList.RenderSize     := Scale96ToScreen(24);
    imSVGListSpec.RenderSize := Scale96ToScreen(64);

    SetLabelConstrains(['+0 000.0 pF**'], [lbValueQ, lbValueR, lbValueLC, lbValueZ, lbValueMF, lbValueEq]);
    SetLabelConstrains(['0:000000:000000**'], [lbValueBUI], False);
    SetLabelConstrains(['+00.0000 mV'], [lbValueVavg, lbValueVd, lbValueVpp, lbValueVrms]);
    SetLabelConstrains(['00 000 000.000 kHz'], [lbValueF, lbValueT]);
    SetLabelConstrains(['000 000 000'], [lbValueD, lbValueN]);
    SetLabelConstrains(['0 000 000 bps**'], [lbValueGV]);

    SetLabelConstrains(TXT_STATUS, [lbConnect], False);

    _h := imSVGList.RenderSize + Scale96ToScreen(10);
    SetToolbarConstrains([tbMode, tbSystem], _h, _h);

    _h := Canvas.GetTextHeight('0') * 170 div 100;
    SetToolbarConstrains([tbSwitchMF, tbSwitchRel, tbSwitchEq], 1, _h);
    SetToolbarConstrains([tbSwitchOffset], Scale96ToScreen(64), _h);
    SetToolbarConstrains([tbSwitchEq], tbSwitchMF.ButtonWidth * 2 - 4, _h);

    _w := Canvas.GetTextWidth('300mV****');
    SetToolbarConstrains([tbSwitchGen], _w, _h);

    SetPanelChildConstrains(pSwitchGenL);

    SetSpeeButtonConstrains([sbRstCnt, sbGenDec, sbGenInc]);
    SetSpeeButtonConstrains([sbStop], sbStop.Canvas.GetTextWidth(sbStop.Caption) + 32);

    SetStatusBarWidth(1, [Format(TXT_LOG_DOING, [TimeToStr(0, appFmtSet)])]);
    SetStatusBarWidth(2, ['FW v0.0']);
    SetStatusBarWidth(3, ['COM000']);
    SetStatusBarWidth(4, TXT_STATUS);
    SetStatusBarWidth(5, ['hold status']);

    cbLanguage.Constraints.MinWidth := Scale96ToScreen(125);
    cbFonts.Constraints.MinWidth    := Scale96ToScreen(125);
    sgRLC.Constraints.MinHeight     := sgRLC.RowHeights[0] * sgRLC.RowCount;
    lsPort.ItemHeight               := Scale96ToScreen(lsPort.Canvas.GetTextHeight('0') + 2);
    pNoPorts.Constraints.MinHeight  := lsPort.ItemHeight * ScaleScreenTo96(6) + 2;
    lbConnect.Constraints.MinWidth  := lbConnect.Constraints.MinWidth + 16;
    lbHold.Font.Height              := Scale96ToScreen(64);
    lbLogged.Font.Height            := Scale96ToScreen(64);

    pConnect.ChildSizing.TopBottomSpacing := Scale96ToScreen(5);
  end;


end.
