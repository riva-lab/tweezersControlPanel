unit fm_calibration;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Forms, ComCtrls, StdCtrls, ExtCtrls, Graphics,
  i18n;

type

  { TfmCalibration }

  TfmCalibration = class(TForm)
    btnOK:       TButton;
    lbHint:      TLabel;
    lbWait:      TLabel;
    pbWork:      TProgressBar;
    pWait:       TPanel;
    tmrProgress: TTimer;

    procedure FormShow(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: char);

    procedure tmrProgressTimer(Sender: TObject);
    procedure btnOKClick(Sender: TObject);

  private
    FAllowClose: Boolean;
    FFirstClick: Boolean;
    FWait:       Boolean;

  public
    procedure AllowClose;
    procedure SetMessage(AHint: String);
    function ContinuedByUser(AForAction: Boolean = False): Boolean;
  end;

var
  fmCalibration: TfmCalibration;

implementation

{$R *.lfm}

{ TfmCalibration }

procedure TfmCalibration.FormShow(Sender: TObject);
  begin
    AutoSize := True;

    lbHint.Constraints.MinHeight := Canvas.GetTextHeight('0') * 3;
    btnOK.Constraints.MinHeight  := Canvas.GetTextHeight('0') * 2;
    pWait.Constraints.MinHeight  := Canvas.GetTextHeight('0');

    AutoSize := False;

    Position    := poMainFormCenter;
    Color       := clMenu;
    FWait       := False;
    FAllowClose := False;
    FFirstClick := True;

    pbWork.Position     := 0;
    pbWork.Style        := pbstMarquee;
    tmrProgress.Enabled := True;
    lbWait.Visible      := False;
    btnOK.Enabled       := False;
    btnOK.Caption       := TXT_WINCAL_GO + ' [Enter]';
  end;

procedure TfmCalibration.FormDeactivate(Sender: TObject);
  begin
    if FAllowClose then Close;
  end;

procedure TfmCalibration.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  begin
    CanClose            := FAllowClose or FFirstClick;
    tmrProgress.Enabled := not CanClose;
  end;

procedure TfmCalibration.FormKeyPress(Sender: TObject; var Key: char);
  begin
    if Key = #27 then Close;
  end;



procedure TfmCalibration.btnOKClick(Sender: TObject);
  begin
    pbWork.Style := pbstNormal;
    FWait        := True;
    FFirstClick  := False;
    if FAllowClose then Close;
  end;

procedure TfmCalibration.tmrProgressTimer(Sender: TObject);
  begin
    lbWait.Visible := FWait;
    if not FAllowClose and FWait then
      with pbWork do
        if Position = Max then Position := 0
        else StepIt;
  end;



procedure TfmCalibration.AllowClose;
  begin
    pbWork.Position := pbWork.Max;
    btnOK.Caption   := TXT_WINCAL_EXIT + ' [Enter]';
    btnOK.Enabled   := True;
    FAllowClose     := True;
  end;

procedure TfmCalibration.SetMessage(AHint: String);
  begin
    FWait := False;
    if AHint <> '' then lbHint.Caption := AHint;
  end;

function TfmCalibration.ContinuedByUser(AForAction: Boolean): Boolean;
  begin
    FWait := not AForAction;

    while not FWait and tmrProgress.Enabled do
      begin
      btnOK.Enabled := True;
      Sleep(5);
      Application.ProcessMessages;
      end;

    btnOK.Enabled := False;
    Result        := tmrProgress.Enabled;
  end;

end.
