﻿tweezersControlPanel
====================

MD/REPO: [English](readme.md) | [**Русский**](readme.ru.md) | [Українська](readme.uk.md)

HTML: [English](readme.html) | [**Русский**](readme.ru.html) | [Українська](readme.uk.html)

---

![](help/light/screenshots/mode-rlc.png)

## О приложении

**tweezersControlPanel** — приложение-контрольная панель для измерительного пинцета типа [BM8232](https://gitlab.com/Andrey_71B/bm8232-rlc-meter-tweezers-hw).

### Выдержка из характеристик пинцета BM8232

1. Измерение параметров пассивного двухполюсника R, L, C и нескольких других.
2. Вольтметр постоянного и переменного тока.
3. Продвинутый частотомер.
4. Проверка полупроводниковых приборов.
5. Простой функциональный генератор сигналов.

### Возможности *tweezersControlPanel*

- полная поддержка всех функций пинцета BM8232,
- автоматический поиск подключенного к хосту пинцета,
- управление режимами работы пинцета,
- удобное отображение измеренных величин,
- поддержка калибровки пинцета,
- функция *Пауза* — замораживание текущих значений,
- запись измеренных величин в файл журнала,
- гибкие настройки внешнего вида и функциональности интерфейса,
- поддержка темной темы,
- поддержка локализации интерфейса.

## Компиляция

Особенности компиляции (со временем могут стать неактуальными):

1. FPC имеет модуль RegExpr, который поставляется с компилятором. Однако обычно он редко обновляется. Обновите его вручную: скопируйте с заменой файлы из `.\libraries\TRegExpr\src\` в `<LAZARUS_DIR>\fpc\<VERSION>\source\packages\regexpr\src\`. Этот модуль используется также в системных модулях, поэтому необходимо обновить скомпилированные объектные файлы. Для этого сделайте следующее:
   - Откройте в IDE проект `testregexpr.lpi` из каталога `<LAZARUS_DIR>\fpc\<VERSION>\source\packages\regexpr\tests\` и скомпилируйте его для всех требуемых целевых платформ, например, для `x86_64-win64` и `i386-win32`. Если сразу не компилируется, закомментируйте строки, на которые указал компилятор, и повторите компиляцию.
   - Перейдите в каталог `<LAZARUS_DIR>\fpc\<VERSION>\source\packages\regexpr\tests\lib\`.
   - Скопируйте с заменой файлы из подкаталогов `<TARGET>` в соответствующие каталоги `<LAZARUS_DIR>\fpc\<VERSION>\units\<TARGET>\regexpr`.

## Локализация

Хотите видеть интерфейс **tweezersControlPanel** на своем родном языке? Присоединяйтесь к сообществу переводчиков **tweezersControlPanel**. Начните переводить, выбрав один из следующих вариантов:

1. Переводите файлы Gettext из репозитория, следуя [инструкции](help/tweezersCP-help.md#помощь-в-локализации-интерфейса) ([.html](help/tweezersCP-help.html#помощь-в-локализации-интерфейса)) в справке.

Перевод будет добавлен в ближайший релиз, если он покрывает не менее 2/3 (~67%).

## Установка

**tweezersControlPanel** может быть установлен как обычное приложение. Также доступна портативная версия, не требующая установки и работающая из любого каталога. Установочные и портативные файлы доступны в разделе [Releases](https://gitlab.com/riva-lab/tweezersControlPanel/-/releases/permalink/latest): так можно получить самую свежую версию.

## Как пользоваться

Руководство пользователя на русском — [help/tweezersCP-help.md](help/tweezersCP-help.md) ([.html](help/tweezersCP-help.html)).

## Ответственность

**tweezersControlPanel** предоставляется для свободного использования, без каких-либо гарантий и технической поддержки. Вы используете приложение по своему усмотрению и несете свою собственную ответственность за результаты его работы.

## Вопросы и предложения

Если Вы обнаружили ошибку в работе приложения или хотите предложить что-то для улучшения приложения, пожалуйста, перейдите в раздел [Задачи](-/issues) проекта **tweezersControlPanel**. Сначала изучите, не открыта ли ранее похожая или такая же задача. Не создавайте дублирующие задачи, обновляйте или переоткрывайте существующие — это ускоряет их рассмотрение. Если Ваш вопрос не поднимался ранее, создавайте новую задачу.

Ваши вопросы и предложения помогают совершенствовать **tweezersControlPanel**.

## Авторство и участие

Copyright 2024 Riva, [FreeBSD License](license.md)

Общая идея приложения **tweezersControlPanel** и аппаратная реализация [измерительного пинцета BM8232](https://gitlab.com/Andrey_71B/bm8232-rlc-meter-tweezers-hw) предложены [a_biv @ list.ru](https://gitlab.com/Andrey_71B/).

[История версий](versions.md)

Разработано в [Free Pascal RAD IDE Lazarus](http://www.lazarus-ide.org) v3.0, компилятор [Free Pascal Compiler](https://freepascal.org) v3.2.2.

Установщик для Windows создан в [Inno Setup](https://jrsoftware.org/isinfo.php). [Copyright](https://jrsoftware.org/files/is/license.txt) (C) 1997-2023, Jordan Russell, Martijn Laan.

Значок установщика: [icon-icons.com](https://icon-icons.com/icon/software/76005), [CC BY 4.0](https://creativecommons.org/licenses/by/4.0).

## Зависимости

- [Ararat Synapse Library](https://github.com/ultibohub/AraratSynapse) Ararat Synapse Library Release 40.1 (Ultibo port of Synapse) 2017-09-29 — the synchronyous socket library. Copyright (c)2001-2011, Lukas Gebauer.
- [controlwindevice unit](https://wiki.lazarus.freepascal.org/Windows_Programming_Tips#Enabling_and_disabling_devices) — enable/disable Windows devices. Copyright (c) 2010-2012 Ludo Brands.
- [TRegExpr](https://github.com/andgineer/TRegExpr) — regular expressions engine in pure Object Pascal. Copyright (c) 1999-2004 Andrey V. Sorokin.
- [metadarkstyle](https://github.com/zamtmn/metadarkstyle) — package that adds dark theme to your program under windows 10. Copyright (c) 2023 zamtmn.
- [BGRABitmap](https://bgrabitmap.github.io/) — a package designed to modify and create images with transparency.
- [BGRA Controls](https://bgrabitmap.github.io/bgracontrols/) — a set of graphical UI elements. Author: Lainz.
- [ImageSVGListDsgn](https://gitlab.com/riva-lab/ImageSVGListDsgn) — a list of SVG images instead of regular bitmaps. Copyright (c) 2023 Riva.
- [OnlineUpdater](https://gitlab.com/riva-lab/OnlineUpdater) — package for updating application from online repository. Copyright (c) 2023 Riva.
