﻿tweezersControlPanel
====================

MD/REPO: [**English**](readme.md) | [Русский](readme.ru.md) | [Українська](readme.uk.md)

HTML: [**English**](readme.html) | [Русский](readme.ru.html) | [Українська](readme.uk.html)

---

![](help/light/screenshots/mode-rlc.png)

## About application

**tweezersControlPanel** — application-control panel for measuring tweezers model [BM8232](https://gitlab.com/Andrey_71B/bm8232-rlc-meter-tweezers-hw/).

### Excerpt from BM8232 tweezers characteristics

1. Measure parameters R, L, C and several others of passive two-pole device.
2. DC and AC voltmeter.
3. Advanced frequency meter.
4. Test of semiconductor devices.
5. Simple functional signal generator.

### Features

- full support for all functions of the BM8232 tweezers,
- automatic search for tweezers connected to the host,
- control of the tweezers' operating modes,
- convenient display of measured values,
- support for tweezers calibration,
- Pause function — freezing of current values,
- recording of measured values in a log file,
- flexible customization of the interface appearance and functionality,
- dark theme support,
- interface localization support.

## Сompilation

Compilation details (may lose relevance over time):

1. FPC has a RegExpr module that comes with the compiler. However, it is usually rarely updated. Update it manually: copy the files with replacement from `.\libraries\TRegExpr\src\` to `<LAZARUS_DIR>\fpc\<VERSION>\source\packages\regexpr\src\`. This module is also used in system modules, so you need to update the compiled object files. To do this, do the following:
   - Open in the IDE the project `testregexpr.lpi` from the catalog `<LAZARUS_DIR>\fpc\<VERSION>\source\packages\regexpr\tests\` and compile it for all required target platforms, e.g. for `x86_64-win64` and `i386-win32`. If it doesn't compile immediately, comment out the lines pointed to by the compiler and compile again.
   - Go to the catalog `<LAZARUS_DIR>\fpc\<VERSION>\source\packages\regexpr\tests\lib\`.
   - Copy the files with replacement from the subdirectories `<TARGET>` to the appropriate catalogs `<LAZARUS_DIR>\fpc\<VERSION>\units\<TARGET>\regexpr`.

## Localization

Want to see the **tweezersControlPanel** interface in your native language? Join the **tweezersControlPanel** community of translators. Start translating by selecting one of the following options:

1. Translate Gettext files from the repository by following the [instructions (in Russian)](help/tweezersCP-help.md#help-in-localization-interface) ([.html](help/tweezersCP-help.html#помощь-в-локализации-интерфейса)) in the help.

The translation will be added in the next release if it covers at least 2/3 (~67%).

## Installation

**tweezersControlPanel** can be installed as a regular application. A portable version is also available, which does not require installation and works from any directory. The installation and portable files are available under [Releases](https://gitlab.com/riva-lab/tweezersControlPanel/-/releases/permalink/latest): this is the best way to get the latest version.

## How to use

User Manual in Russian — [help/tweezersCP-help.md](help/tweezersCP-help.md) ([.html](help/tweezersCP-help.html)).

## Liability

**tweezersControlPanel** is provided for free use, without any warranty or technical support. You use the application at your own discretion and are solely responsible for the results of its functioning.

## Issues and suggestions

If you find a bug in the application or want to suggest something to improve the application, please go to the [Issues](-/issues) section of the **tweezersControlPanel** project. First investigate if a similar or the same issue has been opened before. Do not create duplicate issues, update or reopen existing ones - this speeds up their review. If your issue has not been raised before, create a new issue.

Your questions and suggestions help improve **tweezersControlPanel**.

## Сopyright and credits

Copyright 2024 Riva, [FreeBSD License](license.md)

The general idea of the **tweezersControlPanel** application and the hardware implementation of [BM8232 measuring tweezers](https://gitlab.com/Andrey_71B/bm8232-rlc-meter-tweezers-hw/) are proposed by [a_biv @ list.ru](https://gitlab.com/Andrey_71B/).

[Changelog (Ru)](versions.md)

Developed in [Free Pascal RAD IDE Lazarus](http://www.lazarus-ide.org) v3.0, [Free Pascal Compiler](https://freepascal.org) v3.2.2.

The installer for Windows is created in [Inno Setup](https://jrsoftware.org/isinfo.php). [Copyright](https://jrsoftware.org/files/is/license.txt) (C) 1997-2023, Jordan Russell, Martijn Laan.

Installer icon: [icon-icons.com](https://icon-icons.com/icon/software/76005), [CC BY 4.0](https://creativecommons.org/licenses/by/4.0).

## Dependencies

- [Ararat Synapse Library](https://github.com/ultibohub/AraratSynapse) Ararat Synapse Library Release 40.1 (Ultibo port of Synapse) 2017-09-29 — the synchronyous socket library. Copyright (c)2001-2011, Lukas Gebauer.
- [controlwindevice unit](https://wiki.lazarus.freepascal.org/Windows_Programming_Tips#Enabling_and_disabling_devices) — enable/disable Windows devices. Copyright (c) 2010-2012 Ludo Brands.
- [TRegExpr](https://github.com/andgineer/TRegExpr) — regular expressions engine in pure Object Pascal. Copyright (c) 1999-2004 Andrey V. Sorokin.
- [metadarkstyle](https://github.com/zamtmn/metadarkstyle) — package that adds dark theme to your program under windows 10. Copyright (c) 2023 zamtmn.
- [BGRABitmap](https://bgrabitmap.github.io/) — a package designed to modify and create images with transparency.
- [BGRA Controls](https://bgrabitmap.github.io/bgracontrols/) — a set of graphical UI elements. Author: Lainz.
- [ImageSVGListDsgn](https://gitlab.com/riva-lab/ImageSVGListDsgn) — a list of SVG images instead of regular bitmaps. Copyright (c) 2023 Riva.
- [OnlineUpdater](https://gitlab.com/riva-lab/OnlineUpdater) — package for updating application from online repository. Copyright (c) 2023 Riva.
