﻿tweezersControlPanel
====================

MD/REPO: [English](readme.md) | [Русский](readme.ru.md) | [**Українська**](readme.uk.md)

HTML: [English](readme.html) | [Русский](readme.ru.html) | [**Українська**](readme.uk.html)

---

![](help/light/screenshots/mode-rlc.png)

## Про програму

**tweezersControlPanel** — програма-контрольна панель для вимірювального пінцета типу [BM8232](https://gitlab.com/Andrey_71B/bm8232-rlc-meter-tweezers-hw).

### Витяг із характеристик пінцета BM8232

1. Вимірювання параметрів пасивного двополюсника R, L, C і деяких інших.
2. Вольтметр постійного і змінного струму.
3. Просунутий частотомір.
4. Перевірка напівпровідникових приладів.
5. Простий функціональний генератор сигналів.

### Можливості *tweezersControlPanel*

- повна підтримка всіх функцій пінцета BM8232,
- автоматичний пошук під'єднаного до хосту пінцета,
- управління режимами роботи пінцета,
- зручне відображення виміряних величин,
- підтримка калібрування пінцета,
- функція Пауза - заморожування поточних значень,
- запис виміряних величин у файл журналу,
- гнучкі налаштування зовнішнього вигляду і функціональності інтерфейсу,
- підтримка темної теми,
- підтримка локалізації інтерфейсу.

## Компіляція

Особливості компіляції (з часом можуть стати неактуальними):

1. FPC має модуль RegExpr, який постачається з компілятором. Однак зазвичай він рідко оновлюється. Оновіть його вручну: скопіюйте із заміною файли з `.\libraries\TRegExpr\src\` в `<LAZARUS_DIR>\fpc\<VERSION>\source\packages\regexpr\src\`. Цей модуль використовується також у системних модулях, тому необхідно оновити скомпільовані об'єктні файли. Для цього зробіть наступне:
   - Відкрийте в IDE проект `testregexpr.lpi` з каталогу `<LAZARUS_DIR>\fpc\<VERSION>\source\packages\regexpr\tests\` і скомпілюйте його для всіх необхідних цільових платформ, наприклад, для `x86_64-win64` та `i386-win32`. Якщо відразу не компілюється, закоментуйте рядки, на які вказав компілятор, і повторіть компіляцію.
   - Перейдіть у каталог `<LAZARUS_DIR>\fpc\<VERSION>\source\packages\regexpr\tests\lib\`.
   - Скопіюйте із заміною файли з підкаталогів `<TARGET>` у відповідні каталоги `<LAZARUS_DIR>\fpc\<VERSION>\units\<TARGET>\regexpr`.

## Локалізація

Хочете бачити інтерфейс **tweezersControlPanel** своєю рідною мовою? Приєднуйтесь до спільноти перекладачів **tweezersControlPanel**. Почніть перекладати, обравши один із таких варіантів:

1. Перекладайте файли Gettext із репозиторію, дотримуючись [інструкції (російською)](help/tweezersCP-help.md#помощь-в-локализации-интерфейса) ([.html](help/tweezersCP-help.html#помощь-в-локализации-интерфейса)) у довідці.

Переклад буде додано до найближчого релізу, якщо він покриває щонайменше 2/3 (~67%).

## Встановлення

**tweezersControlPanel** може бути встановлений як типова програма. Також доступна портативна версія, яка не потребує встановлення і працює з будь-якого каталогу. Інсталяційні та портативні файли доступні в розділі [Releases](https://gitlab.com/riva-lab/tweezersControlPanel/-/releases/permalink/latest): так можна отримати найновішу версію.

## Як користуватися

Посібник користувача російською — [help/tweezersCP-help.md](help/tweezersCP-help.md) ([.html](help/tweezersCP-help.html)).

## Відповідальність

**tweezersControlPanel** надається для вільного використання, без будь-яких гарантій і технічної підтримки. Ви використовуєте додаток на свій розсуд і несете свою власну відповідальність за результати його роботи.

## Запитання та пропозиції

Якщо Ви виявили помилку в роботі програми або хочете запропонувати щось для поліпшення програми, будь ласка, перейдіть у розділ [Задачі](-/issues) проекту **tweezersControlPanel**. Спочатку перевірте, чи не відкрито раніше схожу або таку саму задачу. Не створюйте дублюючі задачі, оновлюйте або перевідкривайте наявні - це прискорює їхній розгляд. Якщо Ваше питання не порушувалося раніше, створюйте нову задачу.

Ваші запитання та пропозиції допомагають удосконалювати **tweezersControlPanel**.

## Авторство та участь

Copyright 2024 Riva, [FreeBSD License](license.md)

Загальна ідея програми **tweezersControlPanel** і апаратна реалізація [вимірювального пінцета BM8232](https://gitlab.com/Andrey_71B/bm8232-rlc-meter-tweezers-hw) запропоновані [a_biv @ list.ru](https://gitlab.com/Andrey_71B/).

[Історія версій (рос.)](versions.md)

Розроблено в [Free Pascal RAD IDE Lazarus](http://www.lazarus-ide.org) v3.0, компілятор [Free Pascal Compiler](https://freepascal.org) v3.2.2.

Інсталятор для Windows створений у [Inno Setup](https://jrsoftware.org/isinfo.php). [Copyright](https://jrsoftware.org/files/is/license.txt) (C) 1997-2023, Jordan Russell, Martijn Laan.

Значок інсталятора: [icon-icons.com](https://icon-icons.com/icon/software/76005), [CC BY 4.0](https://creativecommons.org/licenses/by/4.0).

## Залежності

- [Ararat Synapse Library](https://github.com/ultibohub/AraratSynapse) Ararat Synapse Library Release 40.1 (Ultibo port of Synapse) 2017-09-29 — the synchronyous socket library. Copyright (c)2001-2011, Lukas Gebauer.
- [controlwindevice unit](https://wiki.lazarus.freepascal.org/Windows_Programming_Tips#Enabling_and_disabling_devices) — enable/disable Windows devices. Copyright (c) 2010-2012 Ludo Brands.
- [TRegExpr](https://github.com/andgineer/TRegExpr) — regular expressions engine in pure Object Pascal. Copyright (c) 1999-2004 Andrey V. Sorokin.
- [metadarkstyle](https://github.com/zamtmn/metadarkstyle) — package that adds dark theme to your program under windows 10. Copyright (c) 2023 zamtmn.
- [BGRABitmap](https://bgrabitmap.github.io/) — a package designed to modify and create images with transparency.
- [BGRA Controls](https://bgrabitmap.github.io/bgracontrols/) — a set of graphical UI elements. Author: Lainz.
- [ImageSVGListDsgn](https://gitlab.com/riva-lab/ImageSVGListDsgn) — a list of SVG images instead of regular bitmaps. Copyright (c) 2023 Riva.
- [OnlineUpdater](https://gitlab.com/riva-lab/OnlineUpdater) — package for updating application from online repository. Copyright (c) 2023 Riva.
